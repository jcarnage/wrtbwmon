#!/bin/bash -x

# Navigate to the directory that contains this script
cd `dirname $0`
# Ensure that we don't have a previous output file
rm output.txt
# Get our remote file
scp router:/mnt/wrtbwmon/output.txt .
# If we were able to get a file, then let's try 
# and delete it from the remote end
if [ -f "output.txt" ]; then
   ssh router 'rm /mnt/wrtbwmon/output.txt'
   python importData.py
   # rm export.txt
fi

