#!/usr/bin/env python
from __future__ import print_function

import sqlite3
import time
import json
import sys
import getopt
import support

class SqlAccess(object):
    """Class to access our sqlite database."""
    # --------------------------------------------------------------------
    def _create_database(self, connector):
        sql = "CREATE TABLE 'tblIndex' ('indexId' INTEGER PRIMARY KEY, \
        'lastIndex' INTEGER NOT NULL);"
        connector.execute(sql)
        sql = "INSERT INTO tblIndex (lastIndex) VALUES (0)"
        connector.execute(sql)
        connector.commit()

    # --------------------------------------------------------------------
    def _is_table_exists(self, connector):
        """Check if table for storing last index pulled, exists"""
        sql = "SELECT * FROM sqlite_master WHERE type='table' AND name='tblIndex'"
        if (not connector.execute(sql).fetchone()):
            self._create_database(connector)
        else:
            sql = "SELECT * FROM tblIndex WHERE indexId=2"
            cur = connector.cursor()
            cur.execute(sql)
            rows = cur.fetchall()
            if len(rows) == 0:
                sql = "INSERT INTO tblIndex (lastIndex) VALUES (0)"
                connector.execute(sql)
                connector.commit()
                
    # --------------------------------------------------------------------
    def _store_last_index_read(self, connector):
        sql = "UPDATE tblIndex SET lastIndex = %s " % (self.lastIndex)
        sql += "WHERE indexId=2"
        print(sql)
        connector.execute(sql)
        connector.commit()


    # --------------------------------------------------------------------
    def get_last_index(self, connector):
        sql = "SELECT lastIndex from tblIndex WHERE indexId=2"
        cur = connector.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        if len(rows) == 1:
            self.lastIndex = rows[0]

    # -------------------------------------------------------------------------
    def _base_query(self):
        """Build the base query here."""
        sql = "SELECT * FROM tblDailySummary "
        return sql
        

    # -------------------------------------------------------------------------
    def get_previous_data(self):
        """Returns all data."""
        sql =  self._base_query()
        sql += "ORDER BY sumId"
        cur = self.conn.cursor()
        cur.execute(sql)
        myData = []
        rows = cur.fetchall()
        if rows:
            for i in rows:
                myData.append(i[1:])
            idx = len(rows) - 1
            self.lastIndex = rows[idx][0]
            self._store_last_index_read(self.conn)
            return myData
        return None

    # -------------------------------------------------------------------------
    def get_since_last_poll(self):
        """Returns data that exists on date_tuple date."""
        self.get_last_index(self.conn)
        sql = self._base_query()
        sql += "WHERE sumId > %s " % (self.lastIndex)
        sql += "ORDER BY sumId"
        print(sql)
        cur = self.conn.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        myData = []        
        if rows:
            for i in rows:
                myData.append(i[1:])
                self.lastIndex = i[0]
            self._store_last_index_read(self.conn)
            return myData
        return None


    # -------------------------------------------------------------------------
    def __init__(self, dbName=None):
        if dbName is not None:
            self.conn = sqlite3.connect(dbName)
            self.lastIndex = 0
            self._is_table_exists(self.conn)

def helpScreen():
    """Helpscreen to tell the user how to call this module."""
    print ('event_export.py -h [-a|-all] -l')
    print ('-h : help')
    print ('-a : export the entire database (up to yesterday)')
    print ('-l: export new data (since last export')

def main(argv):
    """Run main function of our script."""
    export_all = False
    export_since = False
    sql = SqlAccess("netMonitor.sqlite")
    # yesterday is of format (YY, MM, DD)
    yesterday = support.getDateYesterday()

    try:
        opts, args = getopt.getopt(argv, "hal", ["all"])
    except getopt.GetoptError:
        helpScreen()
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            helpScreen()
            sys.exit()
        elif opt in ("-a", "-all"):
            export_all = True
        elif opt in ("-l"):
            export_since = True

    if export_all is True:
        mylist = sql.get_previous_data()
    elif export_since is True:
        mylist = sql.get_since_last_poll()

    if mylist is not None:
        try:
            with open('summary_export.txt') as infile:
                json_data = json.load(infile)
                json_data.extend(mylist)
        except:
            json_data = []
            json_data.extend(mylist)
        with open('summary_export.txt', 'w') as outfile:
            json.dump(json_data, outfile)        


# Main function call below this point
if __name__ == '__main__':
    main(sys.argv[1:])


