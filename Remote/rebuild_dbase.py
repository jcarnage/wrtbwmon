#!/usr/bin/env python
from __future__ import print_function

import json
import time
import sqlite3

class sqlAccess(object):
    """Class for connecting to our sqlite3 instance."""

    # ********************************************************************************
    # Create the tables for our database
    # ********************************************************************************
    def _create_database(self, connector):
        sql = "CREATE TABLE 'tblData' ('dataId' INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , 'dataIn' INTEGER NOT NULL , 'dataOut' INTEGER,"
        sql = sql + "'dateId' INTEGER NOT NULL , 'lanId' INTEGER NOT NULL, 'macId' INTEGER NOT NULL);"
        connector.execute(sql)
        sql = "CREATE TABLE 'tblDateInfo' ('dateId' INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL, "
        sql = sql + "'EpochSecs' INTEGER NOT NULL UNIQUE);"
        connector.execute(sql)
        sql = "CREATE TABLE 'tblMac' ('macId' INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , 'MAC' CHAR(20) NOT NULL , 'pcName' CHAR(50));"
        connector.execute(sql)
        sql = "CREATE TABLE 'tblDailySummary' ('sumId' INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , 'Year' INTEGER NOT NULL ,"
        sql =  sql + "'Month' INTEGER NOT NULL , 'Day' INTEGER NOT NULL , 'DataIn' INTEGER NOT NULL , 'DataOut' INTEGER NOT NULL )"
        connector.execute(sql)
        sql = "CREATE TABLE 'tblLan' ('lanId' INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , 'lan' CHAR(20) NOT NULL, 'lanName' CHAR(50));" 
        connector.execute(sql)
    # *****************************************************list_items***************************
    # Load the tables into our variables for ease of use
    # ********************************************************************************
    def _loadtables(self):
        cur = self.conn.cursor()
        # Load the user data
        sql = "SELECT macId, MAC from tblMac"
        cur.execute(sql)
        rows = cur.fetchall()
        # Get our user hash information and store it in our dictionary in the
        # form userlist[hash] = id
        if rows:
            for item in rows:
                self.macList[item[1]] = item[0]

        sql =  "SELECT lanId, lanName from tblLan"
        cur.execute(sql)
        rows = cur.fetchall()
        # Get our user hash information and store it in our dictionary in the
        # form userlist[hash] = id
        if rows:
            for item in rows:
                self.lanList[item[1]] = item[0]

        sql =  "SELECT EpochSecs, dateId from tblDateInfo ORDER by dateId DESC LIMIT 1"
        cur.execute(sql)
        rows = cur.fetchall()
        # Get our user hash information and store it in our dictionary in the
        # form userlist[hash] = id
        if rows:
            for item in rows:
                self.dateList[item[0]] = item[1]
        return

    # ********************************************************************************
    # Checks to see if the database exists and if it does not (cannot fetch a record)
    # it calls the appropriate functions to create it.
    # ********************************************************************************
    def _is_table_exists(self, connector):
        sql = "SELECT * FROM sqlite_master WHERE type='table' AND name='tblData'"
        if not connector.execute(sql).fetchone():
            self._create_database(connector)
        else:
            self._loadtables()


    # ********************************************************************************
    # ********************************************************************************
    def _getLastInsertId(self):
        sql = "SELECT last_insert_rowid()"
        cursor = self.conn.execute(sql)
        self.conn.commit()
        for row in cursor:
            lcl_id = row[0]
        return lcl_id

    #********************************************************************************
    #
    #********************************************************************************
    def _get_lan_id(self, lan, lanName):
        """Get the database id for this lan name."""
        if len(self.lanList) > 0 and lanName in self.lanList:
            pass
        else:
            #This lan name does not exist in our database so add it."""
            sql = "INSERT INTO tblLan (lan, lanName) VALUES "
            sql +=  "('%s', '%s')" % (lan, lanName)
            self.conn.execute(sql)
            self.conn.commit()
            self.lanList[lanName] = self._getLastInsertId()

        return self.lanList[lanName]

    #********************************************************************************
    #
    #********************************************************************************
    def _get_mac_id(self, MAC, pcName):
        """Get the database id for this device."""
        if len(self.macList) > 0 and MAC in self.macList:
            pass
        else:
            #This MAC was not found in our list so add it.
            sql = "INSERT INTO tblMac (MAC, pcName) VALUES "
            sql += "('%s', '%s')" % (MAC, pcName)
            self.conn.execute(sql)
            self.conn.commit()
            self.macList[MAC] = self._getLastInsertId()

        return self.macList[MAC]

    #********************************************************************************
    #
    #********************************************************************************
    def _get_epoch_id(self, epochSecs):
        """Get the database id for this epochSecs timeframe."""
        created = False
        if len(self.dateList) > 0 and epochSecs in self.dateList:
            # If the list length is greater than 1 then we must
            # always return a True as this will indicate that this
            # is forcibly new data;  a list > 1 indicates that we
            # have created at least 1 new Epoch Entry 
            if len(self.dateList) > 1:
                created = True
        else:
            #This timeframe was not in our list so add it
            sql = "INSERT INTO tblDateInfo (EpochSecs) VALUES "
            sql += "('%d')" % (int(epochSecs))
            print(sql)
            try:
                self.conn.execute(sql)
                self.conn.commit()
                created = True
                self.dateList[epochSecs] = self._getLastInsertId()
            except Exception, e:
                print("Exception: ", str(e))
                return None, False

        return self.dateList[epochSecs], created

    #********************************************************************************
    #
    #********************************************************************************
    def _insert_entry(self, i):
        """Insert a specific entry into tblData."""
        sql = "INSERT INTO tblData (dataIn, dataOut, lanId, macId, dateId) VALUES  "
        sql += "('%d','%d','%d','%d','%d')" % (i[0], i[1], i[2], i[3], i[4])
        self.conn.execute(sql)
        self.conn.commit()
        self.entries_added += 1

    #********************************************************************************
    #
    #********************************************************************************
    def _get_entry(self, i):
        """Look for a specific entry in our database."""
        cur = self.conn.cursor()
        sql = "SELECT * FROM tblData WHERE lanId=%d AND macId=%d AND dateId=%d" %(i[2],i[3],i[4])
        cur.execute(sql)
        rows = cur.fetchall()
        if len(rows) > 0:
            return True
        else:
            return False

    #********************************************************************************
    #
    #********************************************************************************
    def _add_entry(self, item):
        #try:
        lanId = self._get_lan_id(item[2], item[3])
        macId = self._get_mac_id(item[4], item[5])
        (epochId, added) = self._get_epoch_id(item[6])
        if epochId is not None:
            newEntry = [item[0], item[1], lanId, macId, epochId]
            if added is True:
                self._insert_entry(newEntry)
            else:
                if self._get_entry(newEntry) is False:
                    self._insert_entry(newEntry)
        else:
            print("Did not create epochId %d", item[6])
            
        #except Exception,e:
        #    print ("Error occurred while writing data.  " + str(e))

    # *********************************************************************************
    # Main function that takes a list passed to it and adds it to the database
    # *********************************************************************************
    def process_info(self, list_data):
        # Data is of the format [UserId, User Name, EpochSecs]
        if list_data is not None:
            for i in list_data:
                self._add_entry(i)

        print("Added a total of %d entries out of %d" % \
            (self.entries_added, len(list_data)))

            

    def __init__(self, dbName = None):
        if dbName is not None:
            self.macList = {}
            self.lanList = {}
            self.dateList = {}
            self.conn = sqlite3.connect(dbName)
            self._is_table_exists(self.conn)
            self.entries_added = 0




#
# This program will take an exported sqlite database (in json format) and
# import it into our local MongoDb database.  It will also process the 
# summary data (how many times has a door cycled on a given day/hour)
if __name__ == "__main__":
    # Load file using json.loads
#    try:
    with open("export.txt") as filein:
        DATA = json.load(filein)
    db = sqlAccess("netMonitor.sqlite.new")
    db.process_info(DATA)
#    except Exception, e:
#        print ("Error occurred while processing data.  " + str(e))
