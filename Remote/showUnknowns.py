#!/usr/bin/env python

import cgi,os,sys
import cgitb
import time

sys.path.append('data')
import configFile
import support
import sqlClass
import reportClass

# print an HTTP header
#
def printHTTPheader():
    print "Content-type: text/html"
    print ""
    print ""


def main(tableValues):

    printHTTPheader()

    # this string contains the web page that will be served
    page_str="""
    <head>
        <link type="text/css" rel="stylesheet" href="/netMonitor/stylesheet.css"/>
    	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    </head>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <h1>PAGE_TITLE</h1>

    <div id="table_div" style="border: 1px solid #ccc"></div> 
    <script type='text/javascript'>

    google.charts.load('current', {'packages': ['table']});
    google.charts.setOnLoadCallback(drawTable);

    function drawTable() {
        var data = new google.visualization.DataTable();

        data.addColumn('string', 'Device MAC');
        data.addColumn('date', 'Last Seen Date');
        data.addColumn('string', 'Details Link');
        data.setColumnProperty(2, {allowHtml: true});
        data.addRows([
%s
    ]); """ % (tableValues)

    page_str += """
        var table = new google.visualization.Table(document.getElementById('table_div'));

        function setWidth () {
            $('.google-visualization-table-th:contains(Description)').css('width', '40%');      
        }
        table.draw(data, {allowHtml: true, width: '100%', page: 'enable', pageSize: '20'});
      }  </script> 
    """

    page_str +=  """</body>""" 
    
    # serve the page with the data table embedded in it
    print page_str


if __name__=="__main__":
    db = sqlClass.sqlAccess(configFile.DBASE)
    report = reportClass.sqlReport()
    itemList = db.returnUnknownDevices()
    main(report.printUnknownDevices(itemList))

