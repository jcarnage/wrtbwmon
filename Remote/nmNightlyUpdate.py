#!/usr/bin/env python

import sys, getopt

import sqlClass
import time
import datetime
import reportClass
import configFile as cfg
import support

# This script is run nightly (after midnight) to perform certain updates to 
# the database.   These updates include:
#   - create a daily summary (data in out)
class dateFunctions(object):
    """Methods for handling dates."""
    # *************************************************************************
    def getDateTomorrow(self, (YY, MM, DD)):     
        newDay = datetime.date(YY,MM,DD) + datetime.timedelta(days=1)
        return (newDay.year, newDay.month, newDay.day)

    # *************************************************************************
    def getDateYesterday(self, (YY, MM, DD)):
        if YY != 0 and MM !=0 and DD != 0:
            myDate = datetime.date(YY,MM,DD) - datetime.timedelta(days=1)
        else:
            myDate = datetime.date.today() - datetime.timedelta(1)
        return (myDate.year, myDate.month, myDate.day) 


    # *************************************************************************    
    def getDeltaDays(self, OD, ND):
        """Calculate the delta between two date."""
        d1 = datetime.date(ND[0], ND[1], ND[2])
        d2 = datetime.date(OD[0], OD[1], OD[2])
        myDate = (d1 - d2).days
        return myDate

class sqlClassExtend(sqlClass.sqlAccess):
    """Extending our sql class to perform necessary summary calculations."""

    # ********************************************************************************
    def _getLastEntryDate(self):
        """Return date information for our lastSummaryIndex value."""
        sql = "SELECT Year, Month, Day FROM tblDailySummary ORDER BY sumId DESC LIMIT 1"
        rows = self._readDatabase(sql)
        if rows:
            return ((rows[0][0], rows[0][1], rows[0][2]))
        else:
            return ((0, 0, 0))

    # ********************************************************************************
    def _getDailyData(self, (YY, MM, DD)):
        """Get data for a specific date and calculate totals."""
        itemList = self.returnDayValues(YY, MM, DD, True)
        data = [0, 0]
        if itemList:
            for i in itemList:
                data[0] += i[1]
                data[1] += i[2]
        return data

    # ********************************************************************************
    def _getFirstDateRecord(self):
        """Get the first date record in our database."""
        sql = "SELECT EpochSecs FROM tblDateInfo LIMIT 1"
        data = self._readDatabase(sql)
        if data:
            st = time.localtime(data[0][0])
            return(st.tm_year, st.tm_mon, st.tm_mday)
        else:
            return(0, 0, 0)


    # ********************************************************************************
    def _updateDailySummary(self, YY, MM, DD):
        dataIn, dataOut = self._getDailyData((YY, MM, DD))

        id = self.getDailySummaryId(YY, MM, DD)
        if id == 0:
            sql = "INSERT INTO tblDailySummary (Year, Month, Day, DataIn, DataOut) VALUES "
            sql += "(%d, %d, %d, %d, %d)" % (YY, MM, DD, dataIn, dataOut )            
        else:
            sql = "UPDATE tblDailySummary SET DataIn=%d, DataOut=%d WHERE sumId=%d" % (dataIn, dataOut, id)
        recId = self._writeRecord(sql)
        if id == 0:
            id = recId
        return id

    # ********************************************************************************
    def run(self):
        """Main method within the class."""
        initFlag = False
        # Get yesterday's date
        target_date = self.sp.getDateYesterday((0,0,0))
        # if we previously done a summary update, we start by getting the entry of the
        # last date we processed
        entry_date = self._getLastEntryDate()
        if entry_date == (0, 0, 0):
            initFlag = True
            entry_date = self._getFirstDateRecord()
        # If at this point our date is all 0's we need to exit.    
        if entry_date == (0, 0, 0):
            return 

        # Once we have determined our entry_date (the last date that was processed) we
        # compare it to our target date to determine how big our loop will be
        loop = self.sp.getDeltaDays(entry_date, target_date)
        # if a valid date is found in our summary table, increment it as we want the next
        # date for processing (the one found is already processed info)            
        if initFlag == False:
            entry_date = self.sp.getDateTomorrow(entry_date)

        # Set the index to invalid - this will be useful in the event of a failure to 
        # tell our next run that we were in the middle of something and failed/crashed
        try:
            for i in range(loop):
                self._updateDailySummary(*entry_date)
                entry_date = self.sp.getDateTomorrow(entry_date)
        # Assuming we made it here with no issues go ahead and validate our index
        except Exception, e:
            print("Failed to write all summary data - " + str(e))

    # *******************************************************************************
    def __init__(self, dbName):
        """Initialize our class."""
        self.sp = dateFunctions()
        super(sqlClassExtend, self).__init__(dbName)


##########################################################################
def updateDatabase():
    db = sqlClassExtend(cfg.DBASE)
    db.run()
    db.closeConnection()



if __name__ == "__main__":
    updateDatabase()
