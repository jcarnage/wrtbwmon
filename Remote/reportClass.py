
import locale
import support
import time

class sqlReport(object):

    def getDate(self, epochTime):
        t = time.localtime(epochTime)
        myDate = "new Date(" + str(t.tm_year) + "," + str(t.tm_mon - 1) + "," + str(t.tm_mday) + " )"
        return myDate

    #####################################################
    def printSummary(self, ItemList, tblFormat):
        myVar = ''
        dataInTotal = 0
        dataOutTotal = 0
        dailyOutput = {}
        for row in ItemList:
            if row[3] in dailyOutput:
                thisRow = dailyOutput[row[3]]
                thisRow[0] = thisRow[0] + row[1]
                thisRow[1] = thisRow[1] + row[2]
            else:
                dailyOutput[row[3]] = [row[1],row[2]]

            dataInTotal = dataInTotal + row[1]
            dataOutTotal = dataOutTotal + row[2]

        if (tblFormat == False):                        
            print "Device                  DataOut (KB)     DataIn (KB)"
        else:
            myVar = "\n['Device','DataOut','DataIn'],\n"

        for item in dailyOutput:
            data = dailyOutput[item]
            if (tblFormat == False):
                print ("%-20s %10d %20d") % (item, data[1]/1024, data[0]/1024)
            else:
                myVar = myVar + "['" + item +"'," + str(data[1]/1024) + "," + str(data[0]/1024) + "]"
                myVar = myVar + ",\n"
        
        if (tblFormat == False):
            print ("Totals (MB):   DataOut: %7d    DataIn: %9d ") \
                % (dataOutTotal/1024, dataInTotal/1024)                
        if (myVar):
            return (myVar, dataInTotal, dataOutTotal)
            
#####################################################            
    def printCsvDetails(self,ItemList):           
        for row in ItemList:
            print ("%s,%s,%s,%s") %(row[0], row[1], row[2], row[3])


# *****************************************************************************
# Use this function to put together a list of all the unknown devices and 
# generate hyperlinks that will do something useful (like take us to details
# about the devices in question).
    def printUnknownDevices(self, devices):
        myVar = ''
        for i in devices:
            item = i.split(",")
            myVar += "['%s', %s, '<a href=\"unknownDetails.py?device=%s\" target=\"_blank\">Link</a>']" % \
                     (item[0], self.getDate(int(item[1])), item[0])
            myVar += ",\n" 
        return myVar

# *****************************************************************************
# Use this function to put together a list of all the times and a specific 
# unknown device was connected to our network 
    def printUnknownDeviceDetails(self, records):
        myVar = ''
        for i in records:
            t = time.localtime(i[0])
            myVar += "[new Date(%s, %s, %s, %s, %s), '%02d:%02d','%s', '%s']" %(t.tm_year, (t.tm_mon -1), t.tm_mday, t.tm_hour, t.tm_sec, t.tm_hour, t.tm_sec,i[1], i[2])
            myVar += ",\n"
        return myVar


# *****************************************************************************
# The following two private functions are used by printDailySummary function
# *****************************************************************************
    def _sumItems(self, ItemList):
        lclDict = {}
        time = []
        activeDevices = {}

        for item in ItemList:
            if not item[0] in lclDict:
                lclDict[item[0]] = {}
                time.append(item[0])
                lclDict[item[0]][item[3]] = [item[1], item[2]] # [DataIn, DataOut]
                activeDevices[item[3]] = [""]
            else:
                if not item[3] in lclDict[item[0]]:
                    lclDict[item[0]][item[3]] = [item[1], item[2]] # [DataIn, DataOut]
                    activeDevices[item[3]] = [""]
                else:
                    (i1, i2) = lclDict[item[0]][item[3]]
                    i1 += item[1]
                    i2 += item[2]
                    lclDict[item[0]][item[3]] = [i1, i2]
                    
        return (activeDevices, time, lclDict)

# *****************************************************************************
    def _parseItems(self, ItemList):
        lclDict = {}
        time = []
        activeDevices = {}

        for item in ItemList:
            if not item[0] in lclDict:
                lclDict[item[0]] = {}
                time.append(item[0])
              
            lclDict[item[0]][item[3]] = [item[1], item[2]] # [DataIn, DataOut]
            activeDevices[item[3]] = [""]
        return (activeDevices, time, lclDict)

# *****************************************************************************
# The following is used to generate the networks daily usage table.  The table
# is based on the hour/minute and the information results in stacked columns
#
# Although the output is geared towards google chart, some post processing to 
# remove the [ and ]'s and we have a CSV ready for Excel/Calc
    def printDeviceDailySummary(self, ItemList, devices):
        divisor = 1048576
        lclDict = {}
        activeDevices = {}
        # The following is used to provide ordering for the dictionary
        time = []

        (activeDevices, time, lclDict) = self._parseItems(ItemList)
        # Create the header information
        var = "[ 'Time'" 
        for i in range(0, len(devices)):
            if devices[i][1] in activeDevices:
                var +=  ", '" + devices[i][1] + "'"
        var += " ],\n"
        # Use the time (which is ordered to build our charts in the proper sequence
        for item in time:
            var += "[ '"  + item +"'"
            # For each time value, get the ordered list of the various devices.
            for idx in range(0, len(devices)):
                i = devices[idx][1]
                if i in activeDevices:
                    if i in lclDict[item]:
                        var += ", " + str(lclDict[item][i][0]) # [Only look at dataIn (for now)
                # If a device was not active during this time frame pad it 0 (don't leave empty)
                    else:
                        var += ", 0"
            var += " ],\n"
        return var

# *****************************************************************************
# The following is used to generate the networks daily usage table.  The table
# is based on the hour/minute and the information results in stacked columns
#
# Although the output is geared towards google chart, some post processing to 
# remove the [ and ]'s and we have a CSV ready for Excel/Calc
    def printLanDailySummary(self, ItemList, devices, network = False):
        divisor = 1048576
        lclDict = {}
        activeDevices = {}
        # The following is used to provide ordering for the dictionary
        time = []

        (activeDevices, time, lclDict) = self._sumItems(ItemList) 

        # Create the header information
        var = "[ 'Time'" 
        for i in range(0, len(devices)):
            if devices[i] in activeDevices:
                var +=  ", '" + devices[i] + "_in', '" + devices[i] + "_out'"
        var += " ],\n"
        # Use the time (which is ordered to build our charts in the proper sequence
        for item in time:
            var += "[ '"  + item +"'"
            # For each time value, get the ordered list of the various devices.
            for i in devices:
                if i in activeDevices:
                    if i in lclDict[item]:
                        var += ", " + str(lclDict[item][i][0]) + ", " +  str(lclDict[item][i][1])
                # If a device was not active during this time frame pad it 0 (don't leave empty)
                    else:
                        var += ", 0, 0"
            var += " ],\n"
        return var


# *****************************************************************************
# The following is used to generate the networks monthly usage table
    def printMonthlySummary(self, ItemList, Year, Month):
        daysOfMonth = support.getDaysInMonth(Year, Month)
        thisDay = 1
        divisor = 1024
        dataInTotal = 0
        dataOutTotal = 0
        myVar = "\n['Day','DataIn','DataOut'],\n"
        for row in ItemList:
            while thisDay != row[0]:
                myVar += "['" + str(thisDay) +"',0 , 0],\n"
                thisDay += 1
                if thisDay > daysOfMonth:
                    return (myVar, dataInTotal, dataOutTotal)

            myVar = myVar + "['" + str(row[0]) +"'," + str(row[1]/divisor) + "," + str(row[2]/divisor) + "]"
            myVar = myVar + ",\n"
            dataInTotal = dataInTotal + row[1]
            dataOutTotal = dataOutTotal + row[2]
            thisDay += 1
            if thisDay > daysOfMonth:
                break
        while thisDay <= daysOfMonth:
            myVar += "['" + str(thisDay) +"',0 , 0],\n"
            thisDay += 1
               
        return (myVar, dataInTotal, dataOutTotal)                

##########################################################################
# The following is used to generate the device daily usage table                
    def printDeviceSummaryTable(self, ItemList, hours):
        lclDict = {}
        myVar = ''
        # Prepare out dictionary with base data
        for i in hours:
            lclDict[i] = [0,0]
        # Update our dictionary with real data, and the gaps will be 0,0
        for i in ItemList:
            lclDict[i[0]] = [i[1], i[2]]

        dataInTotal = 0
        dataOutTotal = 0
        myVar = "\n['Time','DataOut','DataIn'],\n"        
        for i in hours:
            j = lclDict[i]
            myVar = myVar + "['" + i +"'," + str(j[1]) + "," + str(j[0]) + "]"
            myVar = myVar + ",\n"
            dataInTotal = dataInTotal + j[0]
            dataOutTotal = dataOutTotal + j[1]
        return (myVar)    
        
    
