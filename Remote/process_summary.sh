#!/bin/bash -x

#python export_summary.py -a
scp summary_export.txt MongoDb:netmonitor/src/.
# If the file was successfully copied we should process it
if [ $? -eq 0 ]; then
	ssh MongoDb 'netmonitor/src/import_summary.sh'
	if [ $? -eq 0 ]; then
		rm export.txt
	fi
fi

