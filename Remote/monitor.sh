#!/bin/sh


APPLICATION=monitor.py

RESULT=`ps -a | sed -n /${APPLICATION}/p`

if [ "${RESULT:-null}" = null ]; then
    cd `dirname $0`
    ./monitor.py &
    logger '\---- monitor.py not running;  started!'
fi

