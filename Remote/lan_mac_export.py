#!/usr/bin/env python
from __future__ import print_function

import sqlite3
#import time
import json
#import sys

class SqlAccess(object):
    """Class to access our sqlite database."""

    # -------------------------------------------------------------------------
    def getMacInfo(self):
        sql = "SELECT MAC, pcName from tblMac WHERE MAC != pcName"
        cur = self.conn.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        return rows

    # -------------------------------------------------------------------------
    def getLanInfo(self):
        sql = "SELECT lan, lanName from tblLan WHERE lan != lanName"
        cur = self.conn.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        return rows
        

    # -------------------------------------------------------------------------
    def run(self):
        lanInfo = self.getLanInfo()
        macInfo = self.getMacInfo()
        return (lanInfo, macInfo)

    # -------------------------------------------------------------------------
    def __init__(self, dbName=None):
        if dbName is not None:
            self.conn = sqlite3.connect(dbName)
            self.lastIndex = 0


# Main function call below this point
if __name__ == '__main__':
    """Run main function of our script."""
    sql = SqlAccess("netMonitor.sqlite")
    # yesterday is of format (YY, MM, DD)

    mylist =  sql.run()    
    if mylist is not None:
        with open('lan_pcname.txt', 'w') as outfile:
            json.dump(mylist, outfile)
            print ("File lan_pcname.txt has been generated" ) 

