import sqlite3
import calendar
import time

newMacs = {}

class sqlAccess(object):
    dbConnect = False
    lastDateId = 0
    conn = 0

#  Consider the following methods private and don't use them for outside the class
    def _create_database(self, connector):
        sql = "CREATE TABLE 'tblData' ('dataId' INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , 'macId' INTEGER NOT NULL ,"
        sql = sql + "'dateId' INTEGER NOT NULL , 'dataIn' INTEGER NOT NULL , 'dataOut' INTEGER, 'lanId' INTEGER NOT NULL);"
        connector.execute(sql)
        sql = "CREATE TABLE 'tblDateInfo' ('dateId' INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , 'EpochSecs' INTEGER NOT NULL);"
        connector.execute(sql)
        sql = "CREATE TABLE 'tblMac' ('macId' INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , 'MAC' CHAR(20) NOT NULL , 'pcName' CHAR(50));"
        connector.execute(sql)
        sql = "CREATE TABLE 'tblDailySummary' ('sumId' INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , 'Year' INTEGER NOT NULL ,"
        sql =  sql + "'Month' INTEGER NOT NULL , 'Day' INTEGER NOT NULL , 'DataIn' INTEGER NOT NULL , 'DataOut' INTEGER NOT NULL )"
        connector.execute(sql)
        sql = "CREATE TABLE 'tblLan' ('lanId' INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL ,'lan' CHAR(20) NOT NULL , 'lanName' CHAR(50));" 
        connector.execute(sql)
        connector.commit()

    # ********************************************************************************
    # Checks to see if the database exists and if it does not (cannot fetch a record)
    # it calls the appropriate functions to create it.
    # ********************************************************************************
    def _is_table_exists(self, connector):
        sql = "SELECT * FROM sqlite_master WHERE type='table' AND name='tblData'"
        if (not connector.execute(sql).fetchone()):
            self._create_database(connector)

    # ********************************************************************************
    # ********************************************************************************
    def _readDatabase(self, sql, delete=False):
        """This is our database handler.  It will perform a read and return all data."""
        try:
            cur = self.conn.cursor()
            cur.execute(sql)
            if delete:
                print(sql)
                self.conn.commit()
                return True
            else:
                rows = cur.fetchall()
                return rows
        except Exception, e:
            print("Error reading from database " + str(e))
            return None

    # ********************************************************************************
    # ********************************************************************************
    def _writeRecord(self, sql):
        """Write a record and return it's id in the table."""
        try:
            self.conn.execute(sql)
            self.conn.commit()
            return self._getLastInsertId()
        except Exception, e:
            print("Error writing record " + str(e))
            return None

    # ********************************************************************************
    # ********************************************************************************
    def _getDateId(self, epochSecs):
        return_val = 0

        sql = "SELECT dateId FROM tblDateInfo WHERE EpochSecs = '%d'" % (epochSecs) 
        cursor = self.conn.execute(sql)
        for row in cursor:
            return_val = row[0]
        return return_val
    #*************************************************************************
    def get_epoch_secs(self, YY=1969, MM=12, DD=31, hh=1, mm=1):
        """Return time in epoch secs when given a specific date time"""
        # A missing variable will return a valid (but likely an incorrect) value
        st = []
        st.extend(time.localtime())
        st[0] = YY
        st[1] = MM
        st[2] = DD
        st[3] = hh
        st[4] = mm
        st[5] = 1 # keep seconds constant
        return int(time.mktime(st))


    #*************************************************************************
    def get_epoch_secs_range(self, date_tuple):
        """Calculate the epoch secs for the last second of the date passed."""
        # Tuple should be of the form (YY, MM, DD)
        st = []
        # If no day is provided (number is 0) then range will be first to last
        # day of the month
        if date_tuple[2] == 0:
            day_range = calendar.monthrange(date_tuple[0], date_tuple[1])        
        st.extend(time.localtime())
        for i in range(0, 3):
            st[i] = date_tuple[i]
        if date_tuple[2] == 0:
            st[i] = day_range[1]
        st[3] = 23
        st[4] = 59
        st[5] = 59
        last_item = int(time.mktime(st))
        if date_tuple[2] == 0:
            st[i] = 1
        st[3] = 0
        st[4] = 0
        st[5] = 0
        first_item = int(time.mktime(st))
        return first_item, last_item

    # ********************************************************************************
    # ********************************************************************************
    def _writeDate(self, epochSecs):
        self.lastDateId = self._getDateId(epochSecs)
        if self.lastDateId == 0:
            sql = "INSERT INTO tblDateInfo (EpochSecs) VALUES ('%d')" % (epochSecs)
            self.lastDateId = self._writeRecord(sql)
        return self.lastDateId
        
    # ********************************************************************************
    # Calling this function will generate a time stamp, write it to the database and then
    # return the index of the new date entry in the database
    # ********************************************************************************
    def _timeStamp(self, epochSecs = None):
        if epochSecs == None:
            myDate = time.localtime()
            return self._writeDate(int(time.mktime(myDate)))
        else:
            return self._writeDate(epochSecs)

    # ********************************************************************************
    # ********************************************************************************
    def _getLastDateId(self):
        return self.lastDateId

    # ********************************************************************************
    # ********************************************************************************   
    def _getDayRange(self, start, end):
        cur = self.conn.cursor()
        sql = "SELECT dateId FROM tblDateInfo WHERE EpochSecs >=%d AND EpochSecs <=%d" % (start, end)
        cur.execute(sql)
        rows = cur.fetchall()
        if rows:
            return (rows[0][0], rows[len(rows)-1][0])
        else:
            return (0, 0)

    # ********************************************************************************
    # ********************************************************************************
    def _getMacId(self, strMac):
        return_val = 0
        sql = "SELECT macId FROM tblMac WHERE MAC = '" + strMac + "'"
        cursor = self.conn.execute(sql)
        for row in cursor:
            return_val = row[0]
        return return_val

    # ********************************************************************************
    # ********************************************************************************
    def _getLanId(self, lan):
        return_val = 0
        sql = "SELECT lanId FROM tblLan WHERE lan = '" + lan + "'"
        cursor = self.conn.execute(sql)
        for row in cursor:
            return_val = row[0]
        return return_val

    # ********************************************************************************
    # ********************************************************************************
    def _getLastInsertId(self):
        sql = "SELECT last_insert_rowid()"
        cursor = self.conn.execute(sql)
        self.conn.commit()
        for row in cursor:
            lcl_id = row[0]
        return lcl_id

    

    # ********************************************************************************
    # ********************************************************************************
    def _writeLan(self, lan, lanName):
        return_val = self._getLanId(lan)
        if return_val == 0:
            sql = "INSERT INTO tblLan (lan, lanName) VALUES ('" + lan + "','" + lanName + "')"
            self.conn.execute(sql)
            self.conn.commit()
            return_val = self._getLastInsertId()
        return return_val


    # ********************************************************************************
    # ********************************************************************************
    def _writeMac(self, macId, pcName):
        return_val = self._getMacId(macId)
        if return_val == 0:
            sql = "INSERT INTO tblMac (MAC, pcName) VALUES ('" + macId + "','" + pcName + "')"
            return_val = self._writeRecord(sql)
            # Call our mailer object to send out email when new MAC is recorded
            if self.mail:
                self.mail.sendMessage(macId)
        return return_val

# End of private methods


    # ********************************************************************************
    # ********************************************************************************
    def getDailySummaryId(self, YY, MM, DD):
        sql = "SELECT sumId FROM tblDailySummary WHERE Year=%d AND Month=%d AND Day=%d" % (YY, MM, DD)
        row = self._readDatabase(sql)
        if row:
            return row[0]
        else:
            return 0


    # ********************************************************************************
    # Returns a list of items with the following layout for each item:
    # (time, dataIn, dataOut, pcName)
    # ********************************************************************************
    def returnDayValues(self, Year, Month, Day, network=False, CSV=True, network_name="All"):
        # print "We are looking for values related to a day"
        (start, end) = self.get_epoch_secs_range((Year, Month, Day))
        (firstId, lastId) = self._getDayRange(start, end)
        if firstId > 0 and lastId > firstId:
            sql = "SELECT strftime('%H:%M',tblDateInfo.EpochSecs,'unixepoch','localtime' ), "
            sql += "tblData.dataIn, tblData.dataOut"

            if not network or network_name != "All":
                sql += ", tblMac.pcName"

            if network or network_name != "All":
                sql += ", tblLan.lanName"

            sql += " FROM tblData "
            sql += "JOIN tblDateInfo "
            sql += "ON tblDateInfo.dateId = tblData.dateId "

            if network or network_name != "All":
                sql += "JOIN tblLan "
                sql += "ON tblLan.lanId = tblData.lanId "

            if not network or network_name != "All":
                sql += "JOIN tblMac "
                sql += "ON tblMac.macId = tblData.macId "
            sql += "WHERE tblDateInfo.dateId BETWEEN %d AND %d" % (firstId, lastId)

            if network_name != "All":
                sql += " AND tblLan.lanName='%s'" % (network_name,)
            rows = self._readDatabase(sql)

            if rows:
                return rows
        return None

    # ********************************************************************************
    # ********************************************************************************
    def getDeviceList(self):
        """Get the device list - using macId and real name."""
        deviceName = {}
        sql = "SELECT macId, pcName FROM tblMac ORDER BY pcName" # COLLATE NOCASE
        rows = self._readDatabase(sql)
        idx = 0
        for row in rows:
            deviceName[idx] = (row[0], str(row[1]))
            idx += 1
        return deviceName

    # ********************************************************************************
    # ********************************************************************************
    def getAllNetworkNames(self, addDefault = True):
        """Get the names of all our networks."""
        devices = []
        if addDefault:
            devices.append("All")
        rows = self._readDatabase("SELECT lanName from tblLan")
        if rows:
            for i in rows:
                devices.append(str(i[0]))
            return devices
        else:
            return None

    # ********************************************************************************
    # ********************************************************************************
    def getAllDeviceNames(self):
        """Get the names of all our devices (known and unknown)."""
        devices = []
        rows = self._readDatabase("SELECT pcName FROM tblMac")
        if rows:
            for i in rows:
                devices.append(str(i[0]))
            return devices
        else:
            return None
        
    # ********************************************************************************
    # ********************************************************************************
    def getAllKnownDeviceNames(self, multiple = False):
        """Get all known device names."""
        devices = []
        rows = self._readDatabase("SELECT pcName, MAC FROM tblMac WHERE MAC != pcName")
        if rows:
            for i in rows:
                # if multiple is set to true, then return the MAC id and pcName
                # TODO:  Why not make this into a tuple??
                if multiple:
                    devices.append(str(i[1])+","+str(i[0]))
                else:
                    devices.append(str(i[0]))
            return devices
        else:
            return None
       
   # ********************************************************************************
    # ********************************************************************************
    def storeData(self, dictData, epochSecs=None):
        # Clear out our newMacs list
        newMacs.clear()
        if dictData:
            #Generate a timestamp for the data we are about to record
            dateIndex = self._timeStamp(epochSecs)
            #Loop through the dictionary data
            for macKey in dictData.keys():
                macIndex = self._writeMac(macKey[0], macKey[0])
                lanIndex = self._writeLan(macKey[1], macKey[1])
                i = dictData[macKey]
                sql = "INSERT INTO tblData (macId, dateId, dataOut, dataIn, lanId) "
                sql += "VALUES (%d, %d, %d, %d, %d) " % (macIndex, dateIndex, i[0], i[1], lanIndex)
                self.conn.execute(sql)
            self.conn.commit()
            return 1
        else:
            return -1
            

    # ********************************************************************************
    # ********************************************************************************
    def getKnownDeviceList(self, Year = 0, Month = 0, Day = 0, Network = ''):
        """Return devices active on a specific date range or network"""
        deviceName = {}
        if (Year == 0 or Month == 0 or Day == 0) and (Network == ''):
            sql  = "SELECT macId, pcName FROM tblMac WHERE MAC != pcName ORDER BY pcName"
        else:
            sql  = "SELECT DISTINCT tblMac.macId, tblMac.pcName FROM tblMac "
            sql += "JOIN tblData "
            sql += "ON tblMac.macId = tblData.macId "
            sql += "JOIN tblDateInfo "
            sql += "ON tblDateInfo.dateId = tblData.dateId "
            if (Network != ''):
                sql += "JOIN tblLan "
                sql += "ON tblLan.lanId = tblData.lanId "
            # Look only for KNOWN devices                
            sql += "WHERE MAC != pcName "
            if (Year == 0 or Month == 0 or Day == 0):
                pass
            else:
                start, end = self.get_epoch_secs_range((Year, Month, Day))
                sql += "AND tblDateInfo.EpochSecs >= '%d' " % (start)
                sql += "AND tblDateInfo.EpochSecs <= '%d'" %(end)

            if (Network != '' and Network != "all"):
                sql += "AND tblLan.lanName = '%s'" % (Network)

        rows = self._readDatabase(sql)
        idx = 0
        for row in rows:
            deviceName[idx] = [row[0], row[1]]
            idx += 1
        return deviceName

    # ********************************************************************************
    # Returns a list of items with the following layout for each item:
    # (time (HH,MM), dataIn, dataOut)
    # ********************************************************************************
    def returnDeviceDailyInfo(self, Year, Month, Day, deviceId):
        """Return data usage for a device on a specific day."""
        (start, end) = self.get_epoch_secs_range((Year, Month, Day))

        sql = "SELECT strftime('%H:%M',tblDateInfo.EpochSecs,'unixepoch','localtime' ),"
        sql += "tblData.dataIn, tblData.dataOut FROM tblData "
        sql += "JOIN tblMac ON tblMac.macId = tblData.macId "
        sql += "JOIN tblDateInfo ON tblDateInfo.dateId = tblData.dateId "
        sql += "WHERE tblDateInfo.EpochSecs BETWEEN "
        sql += "'%d' AND '%d' AND tblMac.macId = '%d'" % (start, end, deviceId)
        rows = self._readDatabase(sql)
        if rows:
            return rows

        return None

    # ***********************************************************************************
    # For any given day, this function will return an ordered list of all times in HH:MM
    # format, that were recorded for that specific date 
    # Returns an array on success, else None is returned
    # ***********************************************************************************
    def getRecordingIntervals(self, Year, Month, Day):
        (start, end) = self.get_epoch_secs_range((Year, Month, Day))
        sql = "SELECT strftime('%H:%M',EpochSecs,'unixepoch','localtime') "
        sql += "FROM tblDateInfo WHERE EpochSecs >=%d AND EpochSecs <=%d" % (start, end)
        rows = self._readDatabase(sql)
        if rows:
            _myArray = []
            for i in rows:
                _myArray.append(str(i[0]))
            return _myArray
        else:
            return None

    # ***********************************************************************************
    def returnMonthlySummary(self, Year, Month):
        sql = "SELECT Day, DataIn, DataOut FROM tblDailySummary "
        sql += "WHERE Year=%d AND Month=%d" % (Year,Month)
        rows = self._readDatabase(sql)
        if rows:
            return rows
        else:
            return None

    # ***********************************************************************************
    def returnMonthValues(self, Year, Month, CSV=True):
        """Montyhly activity for all the devices."""
        # Select all the values for the month

        sql = "SELECT date(tblDateInfo.EpochSecs,'unixepoch','localtime' ), "
        sql += "tblData.dataIn, tblData.dataOut, tblMac.pcName FROM tblData "
        sql += "JOIN tblMac ON tblMac.macId = tblData.macId "
        sql += "JOIN tblDateInfo ON tblDateInfo.dateId = tblData.dateId "
        sql += "WHERE tblDateInfo.EpochSecs BETWEEN '%d' AND '%d'" % (self.get_epoch_secs_range((Year, Month, 0)))
        rows = self._readDatabase(sql)
        if rows:
            return rows
        return None

    # ********************************************************************************
    # ********************************************************************************
    def returnUnknownDevices(self):
        devices = []
        sql = "SELECT tblMac.pcName, tblDateInfo.EpochSecs FROM tblMac "
        sql += "JOIN tblData ON tblData.macId = tblMac.macId "
        sql += "JOIN tblDateInfo ON tblData.dateId = tblDateInfo.dateId "
        sql += "where MAC = pcName GROUP BY tblMac.pcName"
        rows = self._readDatabase(sql)
        if rows:
            for i in rows:
                devices.append(str(i[0]) + "," + str(i[1]))
            return devices
        else:
            return None

    # ********************************************************************************
    # ********************************************************************************
    def returnDeviceHistory(self, macVal):
        records = []

        cur = self.conn.cursor()
        cur.execute("SELECT tblDateInfo.EpochSecs, \
            tblData.dataIn, tblData.dataOut FROM tblData \
            JOIN tblMac \
            ON tblMac.macId = tblData.macId \
            JOIN tblDateInfo \
            ON tblDateInfo.dateId = tblData.dateId \
            WHERE tblMac.MAC = ?", (macVal,));
        rows = cur.fetchall()
        if rows:
            return rows
        return None


    # ********************************************************************************
    # ********************************************************************************
    def closeConnection(self):
        self.dbConnect = False
        self.conn.close()


#    def writeData(self, dictData):

    def __init__(self, dbName, smtpObj = None):
        conn = sqlite3.connect(dbName)
        self.dbConnect = True
        self.conn = conn
        self.mail = smtpObj
        self._is_table_exists(self.conn)


    def __del__(self):
        if (self.dbConnect == True):
            self.conn.close()
#        print "Done"


