#!/usr/bin/env python

import sqlite3

# -------------------------------------------------------------------
if __name__ == "__main__":
    dbName = "netMonitor.sqlite"
    conn = sqlite3.connect(dbName)
    sql = "CREATE TABLE 'tblLan' ('lanId' INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL ,'lan' CHAR(20) NOT NULL , 'lanName' CHAR(50));" 
    conn.execute(sql)
    sql = "ALTER TABLE 'tblData' ADD COLUMN 'lanId' INTEGER;"
    conn.execute(sql)

