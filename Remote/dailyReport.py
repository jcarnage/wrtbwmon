#!/usr/bin/env python

import cgi
import sys
import cgitb
# import time

sys.path.append('data')
import configFile
import support
import sqlClass
import reportClass

# enable tracebacks of exceptions
cgitb.enable()

# print an HTTP header
#
def printHTTPheader():
    print "Content-type: text/html"
    print ""
    print ""


def main(tableValues, Year, Month, Day, networkNameValues, selectedNetname=''):
    searchDate = str(Year) + "-" + str(Month) + "-" + str(Day)

    dropDownItems = ''
    ########  Prepare information for Dropdown ###################
    if networkNameValues is not None:
        for item in networkNameValues:
            # item = networkNames[x]
            if item == selectedNetname:
                # selection = item
                dropDownItems += "<option value=\"" + str(item) + "\" selected>" + item + "</option>\n"
            else:
                dropDownItems += "<option value=\"" + str(item) + "\">" + item + "</option>\n"

    printHTTPheader()

    # this string contains the web page that will be served
    page_str="""
    <head>
	    <link type="text/css" rel="stylesheet" href="/netMonitor/stylesheet.css"/>
    </head>
	

    <h1>PAGE_TITLE</h1>"""

    if tableValues is not None:
        page_str += """
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script type="text/javascript">
          google.load("visualization", "1", {packages:["corechart"]});
          google.setOnLoadCallback(drawChart);
          function drawChart() {
            var data = google.visualization.arrayToDataTable([%s]);

            var options = {
              title: 'Network Daily Usage : %s',
              width: 1100,
              height: 500,
              isStacked: false,
              backgroundColor: '#808090',
              hAxis: {title: 'Date', titleTextStyle: {color: 'white'}},
              vAxis: {title: 'Bandwith (KB)', titleTextStyle: {color: 'white'}, scaleType: 'log'},
              chartArea: {
            		backgroundColor: {
                		stroke: '#fff',
                		strokeWidth: 1
            		}
	            },
              colors: ['green','yellow'],		    	
            };

            var chart = new google.visualization.ColumnChart
		            (document.getElementById('chart_div'));
            chart.draw(data, options);
          }
        </script>
        <div id="chart_div"></div> """ % (tableValues, searchDate)
    else:
        page_str += """
        <div id="error">There is no data for the date provided</div>
        """

    page_str += """	</br></br>
    <div id="form_entry">
    <form action="/cgi-bin/dailyReport.py" method="post">
    Year: <input type="text" name="Year" size=5 maxLength=4 value="%s" />
    Month: <input type="text" name="Month" size=2 maxLength=2 value="%s" />
    Day: <input type="text" name="Day" size=2 maxLength=2 value="%s" />	
    Network: <select name="network"> %s </select>
    <input type="submit" value="Submit" />
    </form>     
    </div>
    """ % (str(Year), str(Month), str(Day), dropDownItems)

    (nYear, nMonth, nDay) = support.yesterday(Year, Month, Day)
    page_str +=  """
    <div id="previous">
    <a id="prevButton" href="/cgi-bin/dailyReport.py?Year=%s&Month=%s&Day=%s">Previous</a> 
    </div>""" % (nYear, nMonth, nDay)

    page_str += """<img id="image" src="/netMonitor/netMonitor.jpg" alt="Boxie" Title="netMonitor"/>
    """

    (nYear, nMonth, nDay) = support.tomorrow(Year, Month, Day)
    page_str +=  """
    <div id="next">
    <a id="nextButton" href="/cgi-bin/dailyReport.py?Year=%s&Month=%s&Day=%s">Next</a> 
    </div>""" % (nYear, nMonth, nDay)

    page_str += """</body>"""

    # serve the page with the data table embedded in it
    print page_str


if __name__ == "__main__":
    db = sqlClass.sqlAccess(configFile.DBASE)
    report = reportClass.sqlReport()
    # Create instance of field storage
    form = cgi.FieldStorage()
    # Get data from the fields
    (yy, mm, dd) = support.getDateToday()

    YY = int(form.getvalue('Year', yy))
    MM = int(form.getvalue('Month', mm))
    DD = int(form.getvalue('Day', dd))
    net = form.getvalue('network', "All")

    networkNames = db.getAllNetworkNames(True)
    itemList = db.returnDayValues(YY, MM, DD, False, network_name=net)
    if itemList is not None:
        (parameters, dataInTotal, dataOutTotal) = report.printSummary(itemList, True)	
        #print parameters
        #print networkNames
        main(parameters, YY, MM, DD, networkNames,  net)
    else:
        main(None, YY, MM,DD, networkNames,  net)
