import time

dflt_sender = 'sender@localhost'
dflt_receiver = 'root@localhost'

dflt_subject = 'New MAC ID:'
dflt_body = 'New Connection occurred at around: '


class Mailer(object):

    smtpObj = ""

    def sendMessage(self, macId, body = dflt_body, subject = dflt_subject):
        body = body + time.asctime(time.localtime(time.time()))
        message = """From: server <%s>
To: netmon <%s>
Subject: %s %s

%s
        """ % (self.sender, self.receiver, subject, macId, body)

        try:
            #print "This is our host %s" % host
            #print "And this is our message %s " % message
            self.smtpObj.sendmail(self.sender, self.receiver, message)
        except:
            if self.logger:
                self.logger.error(("Unable to send email. Message : %s") % (message))


    def __init__(self, logger, smtpObj, sender = dflt_sender, receiver = dflt_receiver):
        self.logger = logger
        self.smtpObj = smtpObj
        self.sender = sender
        self.receiver = receiver
        self.logger.debug("Initialized mailer system")

        
