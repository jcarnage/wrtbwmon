#!/usr/bin/python

import socket
import time, sys
import select
import sqlClass
import logging
import logging.handlers
from mailer import Mailer
import smtplib
from netMonRcv import NetMonitor
from netMonRcv import macList
import config



# TODO:  Put these items in a configuration file
dbName = "netMonitor.sqlite"




# -------------------------------------------------------------------
class trafficRecorder(object):

    mailObj = None

    def dataStore(self, dictList):
        # Add object for mailing 
        q = sqlClass.sqlAccess(dbName, self.mailObj)
        q.storeData(dictList)

    def setupLogging(self):
    ################### Set up our logging ########################
        netLogging = logging.getLogger('monitor.py')
        netLogging.setLevel(logging.DEBUG)

        handler = logging.handlers.SysLogHandler(address = '/dev/log')
        netLogging.addHandler(handler)
        return netLogging
    ###############################################################


    def run(self):
        (port, interval, mailhost, sender_address, receiver_address) = config.loadValues()

        logger = self.setupLogging()
        # Create a listening socket and make sure to seet it as non-blocking
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.setblocking(0)
        s.bind(("", port))

        logger.info(("Netmonitor Started. Waiting on port: %d") % (port))

        if mailhost != None:
            mailer = smtplib.SMTP(mailhost)
            if mailer:
                self.mailObj = Mailer(logger, mailer, sender_address, receiver_address)

        oldTime = int(time.time())
        # Instantiate the monitor class
        netMon = NetMonitor(logger)
        rcvFlag = False
        while 1:
            readable, _, exceptional = select.select([s],[],[s],1)
            if s in readable:
                data, addr = s.recvfrom(1024)
                netMon.parseData(data)
                rcvFlag = True
                time.sleep(1)
            else:
            # If we have received data, we will send it to our debug log and 
            # clear our flag.  This should help with getting data in chunks
            # so that we print it out only once in the log (rather than 
            # multiple times.
                if rcvFlag == True:
                    logger.debug(("Received : %s") % (macList,))
                    rcvFlag = False

                newTime = int(time.time())
                if ((newTime - oldTime) > interval):
                    logger.debug(("900s elapsed....%d") % (newTime))
                    #  This is where we will call our database
                    self.dataStore(macList)
                    macList.clear()
                    oldTime = newTime
                time.sleep(4)

# -------------------------------------------------------------------
if __name__ == "__main__":
    myDaemon = trafficRecorder()
    myDaemon.run()



