#!/bin/bash -x

#python export_data.py -l
scp export.txt MongoDb:netmonitor/src/.
# If the file was successfully copied we should process it
if [ $? -eq 0 ]; then
	ssh MongoDb 'netmonitor/src/import.sh'
	if [ $? -eq 0 ]; then
		rm export.txt
	fi
fi

