#!/usr/bin/env python

from ConfigParser import SafeConfigParser
from ConfigParser import NoOptionError

# -------------------------------------------------------------------
def loadValues():
    #Read our configuration file and set defaults if necessary
    parser = SafeConfigParser()
    parser.read('config.ini')

    try:
        port = (int)(parser.get('config', 'port'))
    except NoOptionError:
        port = 5001

    try:
        interval = (int)(parser.get('config', 'interval'))
    except NoOptionError:
        interval = 900

    try:
        mailhost = parser.get('config','mail_host')
    except NoOptionError:
        mailhost = None

    try:
        sender_address = parser.get('config','sender_address')
    except NoOptionError:
        sender_address = None

    try:
        receiver_address = parser.get('config','receiver_address')
    except NoOptionError:
        receiver_address = None

    return (port, interval, mailhost, sender_address, receiver_address)
