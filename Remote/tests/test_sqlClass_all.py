#!/usr/bin/env python

import test_sqlClass
import test_sqlClass_web
import unittest



if __name__ == '__main__':
    test_classes_to_run = [test_sqlClass.tstSqlClass, test_sqlClass_web.tstSqlClass_web]

    loader = unittest.TestLoader()
    suites_list = []
    for test_class in test_classes_to_run:
        suite = loader.loadTestsFromTestCase(test_class)
        suites_list.append(suite)

    full_suite = unittest.TestSuite(suites_list)
    runner = unittest.TextTestRunner()
    results = runner.run(full_suite)
    
    print(results)
