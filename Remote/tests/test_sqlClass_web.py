#!/usr/bin/env python


import unittest
import os,sys
import mock
import time
sys.path.append('../')
import sqlClass

dbName = "test.sqlite"
macList = {}



#*******************************************************************************
class test_mail(object):
    macId = "N/A"

    def sendMessage(self, macId):
        self.macId = macId

    def getMessage(self):
        return self.macId


#*******************************************************************************
class sqlAccess_ext(sqlClass.sqlAccess):
    """Our extension class which we will use for testing."""

    def getDate(self, dateId):
        """Return EpochSecs found at dateId."""
        sql = "SELECT EpochSecs FROM tblDateInfo WHERE dateId='%d'" % (dateId)
        rows = self._readDatabase(sql)
        return rows[0][0]

    def _addDevices(self):
        """Add devices entries to our database necessary for our tests."""
        try:
            self._writeMac('00:11:22:33:44:55', '00:11:22:33:44:55')
            self._writeMac('00:11:22:33:44:66', '00:11:22:33:44:66')
            self._writeMac('11:22:33:44:55:66', 'macMini')
            self._writeMac('22:33:44:55:66:77', 'devPi')
        except Exception, e:
            print("Error adding device entries " + str(e))

    def _addNetworks(self):
        """Add network entries to our database necessary for our tests."""
        try:
            self._writeLan('eth0.2', 'public')
            self._writeLan('eth0.1', 'private')
        except Exception, e:
            print("Error adding network entries " + str(e))

    def _addDates(self):
        """Add specific dates to our database necessary for our tests."""
        try:
            self._writeDate(self.get_epoch_secs(2017, 5, 7, 1, 10))
            self._writeDate(self.get_epoch_secs(2017, 5, 7, 2, 10))
            self._writeDate(self.get_epoch_secs(2017, 5, 8, 2, 10))
            self._writeDate(self.get_epoch_secs(2017, 6, 7, 2, 10))
        except Exception,e:
            print("Error adding date entries " + str(e))

    @mock.patch.object(sqlClass.sqlAccess,'_timeStamp')
    def _addUnknownDevices(self, mock_timeStamp):
        """Add unknown devices to our data set."""
        data = {}        
        data ['00:11:22:33:44:55','eth0.2'] = [500, 3000]
        data ['00:11:22:33:44:66','eth0.2'] = [500, 3000]
        try:
            mock_timeStamp.return_value = 1 #2017/5/7 @ 1:10am
            self.storeData(data)
        except Exception, e:
            print("Error adding unknown device data entries " + str(e))         


    @mock.patch.object(sqlClass.sqlAccess,'_timeStamp')
    def _addData(self, mock_timeStamp):
        """Add data entries to our database for testing purposes."""
        myList = []
        data = {}        
        data ['11:22:33:44:55:66','eth0.2'] = [100, 2000]
        data ['22:33:44:55:66:77','eth0.1'] = [500, 3000]
        myList.append(data)
        data = {}
        data ['11:22:33:44:55:66','eth0.2'] = [1220, 3000]
        data ['22:33:44:55:66:77','eth0.1'] = [600, 6000]
        myList.append(data)

        try:
            mock_timeStamp.return_value = 1 #2017/5/7 @ 1:10am
            self.storeData(myList[0])
            mock_timeStamp.return_value = 2 #2017/5/7 @ 2:10am
            self.storeData(myList[1])
        except Exception, e:
            print("Error adding data entries " + str(e))                    


    def buildDatabase(self):
        self._addDevices()
        self._addNetworks()
        self._addDates()
        self._addData()

                   

    def __init__(self, dbName):
        super(sqlAccess_ext, self).__init__(dbName)
#*******************************************************************************
class tstSqlClass_web(unittest.TestCase):

    def setUp(self):
        self.q = sqlClass.sqlAccess(dbName)
        pass
    
    def tearDown(self):
        pass

    @classmethod
    def setUpClass(cls):
        try:
            os.remove(dbName)
        except:
            pass        
        q = sqlAccess_ext(dbName)
        q.buildDatabase()
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    # Some tests must occur before others.  We use the _a_, _b_, etc. in the test name to
    # put an order to our tests (they are alphabetical)

    # ====================================================================
    # Test getKnownDeviceList
    def test_10_getKnownDeviceList(self):
        """No specific date is required - return all known devices"""
        devices = self.q.getKnownDeviceList()
        self.assertEqual(devices[0], [4, 'devPi'])        
        self.assertEqual(devices[1], [3, 'macMini'])
        self.assertEqual(2, len(devices))

    def test_12_getKnownDeviceList(self):
        """Show devices connected on a specific network."""        
        devices = self.q.getKnownDeviceList(2017, 5, 7, 'public')       
        self.assertEqual(devices[0], [3, 'macMini'])
        self.assertEqual(1, len(devices))
        
    def test_13_getKnownDeviceList(self):
        """Show devices connected on a specific network."""        
        devices = self.q.getKnownDeviceList(Network = 'private')       
        self.assertEqual(devices[0], [4, 'devPi'])
        self.assertEqual(1, len(devices))

    # ==========================================================================
    # Test returnDeviceDailyInfo
    def test_20_returnDeviceDailyInfo(self):
        """Test what happens when nothing is found."""
        self.assertEqual(None, self.q.returnDeviceDailyInfo(2017, 5, 8, 3))

    def test_21_returnDeviceDailyInfo(self):
        """Test what date is wrong."""
        self.assertEqual(None, self.q.returnDeviceDailyInfo(2017, 5, 0, 1))

    def test_22_returnDeviceDailyInfo(self):
        """Test response to valid data."""
        response = self.q.returnDeviceDailyInfo(2017, 5, 7, 3)
        self.assertEqual("01:10", str(response[0][0]))
        self.assertEqual(2000, response[0][1])
        self.assertEqual(100, response[0][2])
        self.assertEqual("02:10", str(response[1][0]))
        self.assertEqual(3000, response[1][1])
        self.assertEqual(1220, response[1][2])


    # ==========================================================================
    # Test getRecordingIntervals
    def test_30_getRecordingInterals(self):
        """Test what happens when nothing is found."""
        self.assertEqual(None, self.q.getRecordingIntervals(2017, 5, 9))

    def test_31_getRecordingInterals(self):
        """Test what happens when the date is wrong."""
        self.assertEqual(None, self.q.getRecordingIntervals(2017, 0, 9))
           
    def test_32_getRecordingInterals(self):
        """Test what happens when the date is wrong."""
        res = self.q.getRecordingIntervals(2017, 5, 7)
        self.assertEqual(2, len(res))
        self.assertEqual("01:10", res[0])
        self.assertEqual("02:10", res[1])

    # ==========================================================================
    # Test returnMonthlySummary

    # ==========================================================================
    # Test returnMonthValues
    def test_50_returnMonthValues(self):
        """Return an invalid set of results."""
        self.assertEqual(None, self.q.returnMonthValues(2017, 3))

    def test_51_returnMonthValues(self):
        """Return a valid set of results."""
        res = self.q.returnMonthValues(2017, 5)
        self.assertEqual(4, len(res))

    # ==========================================================================
    # Test returnUnknownDevices
    def test_60_returnUnknownDevices(self):
        """Return an invalid set of results."""
        res = self.q.returnUnknownDevices()
        self.assertEqual(None, res)

    def test_61_returnUnknownDevices(self):
        """Return a valid set of results."""
        q = sqlAccess_ext(dbName)
        q._addUnknownDevices()
        res = self.q.returnUnknownDevices()
        self.assertEqual(2, len(res))
        self.assertEqual(res[0], "00:11:22:33:44:55,1494133801")
        self.assertEqual(res[1], "00:11:22:33:44:66,1494133801")

    # =========================================================================
    # Verify thatreturnMonthValues doesn't discrimanate against unknown devices
    def test_62_returnMonthValues(self):
        """Return a valid set of results."""
        res = self.q.returnMonthValues(2017, 5)
        self.assertEqual(6, len(res))


    # ==========================================================================
    # Test returnDeviceHistory




        

if __name__ == '__main__':
    test_classes_to_run = [tstSqlClass_web]

    loader = unittest.TestLoader()
    suites_list = []
    for test_class in test_classes_to_run:
        suite = loader.loadTestsFromTestCase(test_class)
        suites_list.append(suite)

    full_suite = unittest.TestSuite(suites_list)
    runner = unittest.TextTestRunner()
    results = runner.run(full_suite)
    
    print(results)
