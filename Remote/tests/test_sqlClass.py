#!/usr/bin/env python


import unittest
import os,sys
import mock
import time
sys.path.append('../')
import sqlClass

dbName = "test.sqlite"
macList = {}

def buildDatabase():
    try:
        os.remove(dbName)
    except:
        db = sqlAccess_ext(dbName)
        db.closeConnection()
        pass

#*******************************************************************************
class test_mail(object):
    macId = "N/A"

    def sendMessage(self, macId):
        self.macId = macId

    def getMessage(self):
        return self.macId


#*******************************************************************************
class sqlAccess_ext(sqlClass.sqlAccess):
    """Our extension class which we will use for testing."""

    def getDate(self, dateId):
        """Return EpochSecs found at dateId."""
        sql = "SELECT EpochSecs FROM tblDateInfo WHERE dateId='%d'" % (dateId)
        rows = self._readDatabase(sql)
        return rows[0][0]

    def __init__(self, dbName):
        super(sqlAccess_ext, self).__init__(dbName)
#*******************************************************************************
class tstSqlClass(unittest.TestCase):

    def setUp(self):
        pass
    
    def tearDown(self):
        pass

    @classmethod
    def setUpClass(cls):
        buildDatabase()
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    # Some tests must occur before others.  We use the _a_, _b_, etc. in the test name to
    # put an order to our tests (they are alphabetical)
    def test_Close(self):
        q = sqlClass.sqlAccess(dbName)
        self.assertEqual(q.dbConnect, True)
        q.closeConnection()
        self.assertEqual(q.dbConnect, False)

    # ====================================================================
    # Test our _writeMac function
    def test_010_writeMac(self):
        """A new entry will result in an email being sent."""
        mailer = test_mail()
        q = sqlClass.sqlAccess(dbName, mailer)
        self.assertEqual(q._writeMac("00:11:22:33:FF:EE","macMini"),1)
        self.assertEqual(mailer.getMessage(),'00:11:22:33:FF:EE')
        q.closeConnection()  

    def test_011_writeMacDuplicate(self):
        """A duplicate entry will not be written and no email will be sent."""
        mailer = test_mail()
        q = sqlClass.sqlAccess(dbName, mailer)
        self.assertEqual(q._writeMac("00:11:22:33:FF:EE","macMini"),1)
        self.assertEqual(mailer.getMessage(),'N/A')
        q.closeConnection()      

    # ====================================================================
    # Test our _writeLan function
    def test_015_writeLan(self):
        """Confirm new and duplicate entries are properly handled."""
        q = sqlClass.sqlAccess(dbName)
        self.assertEqual(q._writeLan("br-lan","br-lan"),1)
        self.assertEqual(q._writeLan("br-lan","br-lan"),1)
        q.closeConnection()      

    # ====================================================================
    # Test our _writeDate function
    def test_020_writeDate(self):
        q = sqlClass.sqlAccess(dbName)
        # YEAR, MONTH, DAY, HOUR, MINUTE, SECOND, WDAY, YDAY, DST)
        ret = time.mktime((2018, 7, 20, 11, 30, 21, 3, 207, 1))
        self.assertEqual(q._writeDate(ret),1)
        self.assertEqual(q._getLastDateId(),1)
        q.closeConnection() 

    # ====================================================================
    # Test our getDailySummaryId function
    @mock.patch.object(sqlClass.sqlAccess, '_readDatabase')
    def test_035_getSummaryId(self, mock_readDB):
        q = sqlClass.sqlAccess(dbName)
        mock_readDB.return_value = [1]
        self.assertEqual(q.getDailySummaryId(2016,2,23), 1)
        # This database doesn't write the summary data so we must confirm
        # that the SQL string being sent to the _readDatabase method is correct
        mock_readDB.assert_called_once_with("SELECT sumId FROM tblDailySummary WHERE Year=2016 AND Month=2 AND Day=23")

    # ====================================================================
    # Test our returnMonthlySummary function
    @mock.patch.object(sqlClass.sqlAccess, '_readDatabase')
    def test_040_returnMonthlySummary(self, mock_readDB):
        q = sqlClass.sqlAccess(dbName)
        mock_readDB.return_value = ((2, 3000, 4000), (3, 3100, 4200))
        items = q.returnMonthlySummary(2016 ,2)
        self.assertEqual(2, len(items))

    # ====================================================================
    # Confirm that our _timeStamp function works correctly
    @mock.patch.object(time, 'localtime')
    def test_050_timeStamp(self, mock_localTime):
        q = sqlAccess_ext(dbName)
        dateTuple = (2018, 7, 20, 11, 35, 21, 3, 207, 1)
        mock_localTime.return_value = (dateTuple)
        self.assertEqual(q._timeStamp(), 2)
        self.assertEqual(q._getLastDateId(),2)
        # Confirm the correct value was written in the database
        self.assertEqual(int(time.mktime(dateTuple)), q.getDate(2))

    # ====================================================================
    # Test our _getMacId method
    def test_060_getMacId(self):
        q = sqlClass.sqlAccess(dbName)
        r = q._getMacId("00:11:22:33:FF:EE")
        self.assertEqual(r,1)

    # ====================================================================
    # Test our _getDayRange 
    def test_065_getDateRange(self):
        q = sqlClass.sqlAccess(dbName)
        q._writeDate(q.get_epoch_secs(2018, 7, 21, 11, 40))
        q._writeDate(q.get_epoch_secs(2018, 7, 22, 11, 45))
        q._writeDate(q.get_epoch_secs(2018, 7, 22, 11, 50))
        q._writeDate(q.get_epoch_secs(2018, 7, 22, 11, 55))
        q._writeDate(q.get_epoch_secs(2018, 7, 23, 12, 00))
        q._writeDate(q.get_epoch_secs(2018, 7, 24, 12, 40))
        q._writeDate(q.get_epoch_secs(2018, 7, 25, 11, 40))
        q._writeDate(q.get_epoch_secs(2018, 7, 26, 11, 40))
        # Unable to find range returns 0,0
        start, stop = q.get_epoch_secs_range((2018, 8, 22))
        self.assertEqual(q._getDayRange(start, stop), (0,0))
        # Found range returns start and end id
        start, stop = q.get_epoch_secs_range((2018, 7, 22))
        self.assertEqual(q._getDayRange(start, stop),(4,6))

    # ====================================================================
    # Test our _getDeviceList method
    def test_070_getDeviceList(self):
        """Verify we pick up the correct amount of items."""
        q = sqlClass.sqlAccess(dbName)
        # Put some random macId's in the database (_writeMac is previously tested)
        q._writeMac("00:11:22:33:10:EE","Device2")
        q._writeMac("00:11:22:33:11:EE","Device3")
        q._writeMac("00:11:22:33:12:EE","Device4")
        dictNames = q.getDeviceList()
        self.assertEqual(len(dictNames), 4)
        self.assertEqual(dictNames[0], (2, 'Device2') )

    # ====================================================================
    # Test our getAllDeviceNames method
    def test_071_getDeviceNames(self):
        q = sqlClass.sqlAccess(dbName)
        deviceNames = q.getAllDeviceNames()
        self.assertEqual(len(deviceNames), 4)
        self.assertEqual(deviceNames[0], "macMini")
        self.assertEqual(deviceNames[1], "Device2")
        self.assertEqual(deviceNames[2], "Device3")
        self.assertEqual(deviceNames[3], "Device4")       

    # ====================================================================
    # Test our storeData method
    def test_080_storeEmptyData(self):
        """Attempt to store an empty data set."""
        q = sqlClass.sqlAccess(dbName)
        self.assertEqual(q.storeData(macList), -1)

    def test_085_storeData(self):
        """Store two entries for unknown Mac IDs."""
        macList["00:11:22:33:44:55","eth0.3"] = [500, 1200]
        macList["01:12:23:34:45:56","eth0.3"] = [1500, 12200]
        q = sqlClass.sqlAccess(dbName)
        # Confirm that we only have 4 devices and 2 lan entries
        self.assertEqual(4, len(q.getAllDeviceNames()))
        self.assertEqual(2, len(q.getAllNetworkNames()))
        # Execute the tests
        self.assertEqual(q.storeData(macList), 1)
        # Verify the results
        deviceNames = q.getAllDeviceNames()
        lanNames = q.getAllNetworkNames()
        self.assertEqual(len(deviceNames), 6)
        self.assertEqual(deviceNames[5], "00:11:22:33:44:55")
        self.assertEqual(deviceNames[4], "01:12:23:34:45:56")  
        self.assertEqual(len(lanNames), 3)
        self.assertEqual(lanNames[2], "eth0.3")

    # ====================================================================
    # Test our getAllKnownDevicenames method
    def test_090_getKnownDeviceNames(self):
        q = sqlClass.sqlAccess(dbName)
        # Confirm that there are more than 4 names in the database
        deviceNames = q.getAllDeviceNames()
        self.assertEqual(len(deviceNames), 6)
        # Call the function under test to ensure we get only the known devices
        deviceNames = q.getAllKnownDeviceNames()
        self.assertEqual(len(deviceNames), 4)
        self.assertEqual(deviceNames[0], "macMini")
        self.assertEqual(deviceNames[1], "Device2")
        self.assertEqual(deviceNames[2], "Device3")
        self.assertEqual(deviceNames[3], "Device4")

    # ====================================================================
    # Test our getAllNetworkNames method
    def test_091_getKnownNetworkNames(self):
        q = sqlClass.sqlAccess(dbName)
        netNames = q.getAllNetworkNames()
        # Confirm that there are 2 network names (the first is the ALL value)
        self.assertEqual(len(netNames), 3)
        self.assertEqual(netNames[0], "All")
        self.assertEqual(netNames[1], "br-lan")
        self.assertEqual(netNames[2], "eth0.3")

    # ====================================================================
    def test_095_getKnownDeviceList(self):
        q = sqlClass.sqlAccess(dbName)
        dictNames = q.getKnownDeviceList()
        self.assertEqual(len(dictNames), 4)

    # ====================================================================
    def test_100_returnDayValues(self):
        q = sqlClass.sqlAccess(dbName)
        self.assertEqual(q.returnDayValues(2018,7,21,True),None)

    # ====================================================================        
    def test_105_returnDeviceDailyInfoValues(self):
        q = sqlClass.sqlAccess(dbName)
        self.assertEqual(q.returnDeviceDailyInfo(2017,15,1,2),None)

    # ====================================================================
    def test_110_getRecordingIntervals(self):
        q = sqlClass.sqlAccess(dbName)
        values = q.getRecordingIntervals(2018,7,22)
        self.assertEqual(3, len(values))
        self.assertEqual(values[0], "11:45")
        self.assertEqual(values[1], "11:50")
        self.assertEqual(values[2], "11:55")

    # ====================================================================
    def test_115_returnMonthValues(self):
        q = sqlClass.sqlAccess(dbName)
        self.assertEqual(q.returnMonthValues(2017,5,True),None)
        self.assertEqual(q.returnMonthValues(2016,4,True),None)

    # ====================================================================
    @mock.patch.object(sqlClass.sqlAccess, '_readDatabase')
    def test_200_getKnownDeviceList(self, mock_readDB):
        """No specific date is required"""
        q = sqlClass.sqlAccess(dbName)
        mock_readDB.return_value = ((1, "miniMac"), (2, "devPi"))
        devices = q.getKnownDeviceList()
        self.assertEqual(devices[0], [1, 'miniMac'])
        self.assertEqual(devices[1], [2, 'devPi'])
        sql = "SELECT macId, pcName FROM tblMac WHERE MAC != pcName ORDER BY pcName"
        mock_readDB.assert_called_once_with(sql)


        

if __name__ == '__main__':
    test_classes_to_run = [tstSqlClass]

    loader = unittest.TestLoader()
    suites_list = []
    for test_class in test_classes_to_run:
        suite = loader.loadTestsFromTestCase(test_class)
        suites_list.append(suite)

    full_suite = unittest.TestSuite(suites_list)
    runner = unittest.TextTestRunner()
    results = runner.run(full_suite)
    
    print(results)
