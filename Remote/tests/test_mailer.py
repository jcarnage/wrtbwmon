#!/usr/bin/env python

import mock,unittest,sys

sys.path.append('../')

from mailer import *

#*******************************************************************************
class test_log(object):
    
    testString = ""
    
    def debug(self, myString):
        self.testString = myString

    def info(self, myString):
        self.testString = myString

    def error(self, myString):
        self.testString = myString

    def getTestString(self):
        return self.testString

#*******************************************************************************
class test_smtp(object):
    sender = ""
    receiver = ""
    message = ""

    def sendmail(self, sender, receiver, message):
        self.sender = sender
        self.receiver = receiver
        self.message = message

    def getSender(self):
        return self.sender

    def getReceiver(self):
        return self.receiver

    def getMessage(self):
        return self.message


#*******************************************************************************
class test_smtpExcept(object):

    def sendmail(self, sender, receiver, message):
        raise Exception('Unable to send mail')   


#*******************************************************************************
class test_mailer(unittest.TestCase):

    logger = test_log()


#   def tearDown(self):

    def test_sendMessage(self):
        smtpObj = test_smtp()
        myMailer = Mailer(self.logger, smtpObj)
        myMailer.sendMessage('00:AA:BB:CC:DD:EE')
        self.assertEqual(smtpObj.getSender(),"sender@localhost")
        self.assertEqual(smtpObj.getReceiver(),"root@localhost")
        message = smtpObj.getMessage()
        self.assertEqual(message[:135],'From: server <sender@localhost>\nTo: netmon <root@localhost>\nSubject: New MAC ID: 00:AA:BB:CC:DD:EE\n\nNew Connection occurred at around: ')

    def test_sendMessage_1(self):
        smtpObj = test_smtp()
        myMailer = Mailer(self.logger, smtpObj, "sender@someip.com", "receiver@anotherip.com")
        myMailer.sendMessage('00:AA:BB:CC:DD:EE')
        self.assertEqual(smtpObj.getSender(),"sender@someip.com")
        self.assertEqual(smtpObj.getReceiver(),"receiver@anotherip.com")
        message = smtpObj.getMessage()
        self.assertEqual(message[:144],'From: server <sender@someip.com>\nTo: netmon <receiver@anotherip.com>\nSubject: New MAC ID: 00:AA:BB:CC:DD:EE\n\nNew Connection occurred at around: ')


    def test_exception(self):
        smtpObj = test_smtpExcept()
        myMailer = Mailer(self.logger, smtpObj)
        myMailer.sendMessage('00:AA:BB:CC:DD:EE')
        message = self.logger.getTestString()
        self.assertEqual(message[:63],'Unable to send email. Message : From: server <sender@localhost>')

#*******************************************************************************
if __name__ == '__main__':
    unittest.main()
