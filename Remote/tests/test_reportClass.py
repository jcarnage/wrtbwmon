#!/usr/bin/env python

import unittest,sys

sys.path.append('../')

from reportClass import *

class test_reportClass(unittest.TestCase):

    logger = None

    
#    def tearDown(self):
#        macList.clear()

    def test_00_sumItems(self):
        dut = sqlReport()
        itemList = [(u'00:11', 9506, 22832, u'eth0.3')]
        itemList.append((u'00:11', 13281, 9469, u'eth0.4'))
        itemList.append((u'00:11', 52, 40, u'eth0.4'))
        itemList.append((u'00:11', 173252, 49513, u'br-lan'))
        itemList.append((u'00:11', 19417, 6143, u'br-lan'))
        itemList.append((u'00:11', 26008, 141263, u'eth0.3'))
        itemList.append((u'00:11', 14233, 3538, u'eth0.4'))
        (activeDevices, time, lclDict) = dut._sumItems(itemList)
        self.assertEqual(3, len(activeDevices))
        self.assertEqual(1, len(lclDict))
        self.assertEqual(1, len(time))


    def test_10_parseItems(self):
        dut = sqlReport();
        itemList = [(u'00:05', 18591, 29057, u'Echo')]
        itemList.append((u'00:05', 10296, 6670, u'RaspPi'))
        itemList.append((u'00:05', 20673, 7657, u'iPhone_JT'))
        itemList.append((u'00:10', 13086, 10359, u'Matthews_MBP'))
        itemList.append((u'00:10', 76, 76, u'AMD_LP_Desktop'))
        itemList.append((u'00:10', 60995, 8341, u'LivingRoom-AppleTV'))
        (activeDevices, time, lclDict) = dut._parseItems(itemList)
        self.assertEqual(6, len(activeDevices))
        self.assertEqual(2, len(lclDict))
        self.assertEqual(2, len(time))
     
    def test_50_printDeviceDailySummary(self):
        dut = sqlReport();
        itemList = [(u'00:05', 18591, 29057, u'Echo')]
        itemList.append((u'00:05', 10296, 6670, u'RaspPi'))
        itemList.append((u'00:05', 20673, 7657, u'iPadProJT'))
        itemList.append((u'00:10', 13086, 10359, u'Echo'))
        itemList.append((u'00:10', 76, 76, u'iPadProJT'))
        itemList.append((u'00:10', 60995, 8341, u'RaspPi'))
        devices = {}
        devices[0] = [27, u'Echo']
        devices[1] = [1, u'RaspPi']
        devices[2] = [58, u'iPadProJT']
        var = dut.printDeviceDailySummary(itemList, devices)
        self.assertEqual(var[0:42],u"[ 'Time', 'Echo', 'RaspPi', 'iPadProJT' ],")
        self.assertEqual(var[43:76],u"[ '00:05', 18591, 10296, 20673 ],")
        self.assertEqual(var[77:107],u"[ '00:10', 13086, 60995, 76 ],")

if __name__ == '__main__':
    unittest.main()
