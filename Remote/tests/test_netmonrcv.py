#!/usr/bin/env python

import unittest,sys

sys.path.append('../')

from netMonRcv import *

class test_netmonrcv(unittest.TestCase):

    logger = None

    def tearDown(self):
        macList.clear()

    def test_00_splitData(self):
        netMon = NetMonitor(self.logger)
        values = ["00:0C:29:E5:4C:A2", 360, 12560, "N/A"]
        self.assertEqual(netMon.splitData("00:0C:29:E5:4C:A2,360,12560"), values)

    def test_splitDataWithLan(self):
        netMon = NetMonitor(self.logger)
        values = ["00:0C:29:E5:4C:A2", 360, 12560, "eth0.4"]
        self.assertEqual(netMon.splitData("00:0C:29:E5:4C:A2,360,12560,eth0.4"), values)

    def test_splitDataFail(self):
        netMon = NetMonitor(self.logger)      
        self.assertEqual(netMon.splitData("00:0C:29:E5:4C:A2,360,"), None)

    def test_splitData(self):
        netMon = NetMonitor(self.logger)
        self.assertEqual(netMon.splitData(""), None)
        
    def test_10_addToDict(self):
        netMon = NetMonitor(self.logger)
        value = ["00:0C:29:E5:4C:A2", 360, 12560, "br-lan"]
        netMon.addToDict(value)
        self.assertEqual(macList["00:0C:29:E5:4C:A2","br-lan"],[360,12560]) 

    def test_11_addToDictWithLan(self):
        netMon = NetMonitor(self.logger)
        value = ["00:0C:29:E5:4C:A3", 360, 12560, "br-lan"]
        netMon.addToDict(value)
        self.assertEqual(macList["00:0C:29:E5:4C:A3","br-lan"],[360,12560])

    def test_12_updateDictFail(self):
        netMon = NetMonitor(self.logger)
        value = ["00:0C:29:E5:4C:A2", 360]
        netMon.addToDict(value)
        self.assertEqual(len(macList), 0)
        value = ["00:0C:29:E5:4C:A2", 360, 150, "eth0.3", 1501]
        netMon.addToDict(value)
        self.assertEqual(len(macList), 0)

    def test_13_updateDict(self):
        netMon = NetMonitor(self.logger)
        value = ["00:0C:29:E5:4C:A2", 360, 12560, "br-lan"]
        netMon.addToDict(value)
        netMon.addToDict(value)
        self.assertEqual(macList["00:0C:29:E5:4C:A2","br-lan"],[720,25120])

    def test_14_updateDictMultValues(self):
        netMon = NetMonitor(self.logger)
        value = ["00:0C:29:E5:4C:A2", 360, 12560,"br-lan"]
        netMon.addToDict(value)
        netMon.addToDict(value)
        value = ["00:0C:29:E5:4C:A3", 360, 12560,"N/A"]
        netMon.addToDict(value)
        self.assertEqual(len(macList),2)
        self.assertEqual(macList["00:0C:29:E5:4C:A2","br-lan"],[720,25120]) 
        self.assertEqual(macList["00:0C:29:E5:4C:A3","N/A"],[360,12560])

    def test_parseData(self):
        netMon = NetMonitor(self.logger)
        value = "00:0C:29:E5:4C:A2,362,12561,eth0.4\n10:22:33:44:AA:BB,360,12560,br-lan\n\n"
        netMon.parseData(value)
        self.assertEqual(len(macList),2)
        self.assertEqual(macList["00:0C:29:E5:4C:A2","eth0.4"],[362,12561]) 
        self.assertEqual(macList["10:22:33:44:AA:BB","br-lan"],[360,12560]) 

    def test_parseDataList(self):
        netMon = NetMonitor(self.logger) 
        value = ['b8:27:eb:f2:e0:33,28132,47167,eth0.3\n94:de:80:ac:19:5e,50715,1028493,eth0.3\n10:1c:0c:44:ca:9d,404,591,br-lan\nb4:ae:2b:67:62:ca,2945161,22555608,br-lan\n3c:a9:f4:38:76:0c,1654860,7959085,eth0.3\nc0:1a:da:c0:36:c9,1324,521,br-lan\n']
        netMon.parseData(value, False)
        self.assertEqual(len(macList),6)
        self.assertEqual(macList["b8:27:eb:f2:e0:33","eth0.3"],[28132,47167]) 
        self.assertEqual(macList["b4:ae:2b:67:62:ca","br-lan"],[2945161,22555608])
        
    def test_parseDataMultiItemList(self):
        netMon = NetMonitor(self.logger) 
        value = ['b8:27:eb:f2:e0:33,28132,47167\n','94:de:80:ac:19:5e,50715,1028493\n','10:1c:0c:44:ca:9d,404,591\n','b4:ae:2b:67:62:ca,2945161,22555608,eth0.3\n','3c:a9:f4:38:76:0c,1654860,7959085\n','c0:1a:da:c0:36:c9,1324,521\n']
        netMon.parseData(value, False)
        self.assertEqual(len(macList),6)
        self.assertEqual(macList["b8:27:eb:f2:e0:33", "N/A"],[28132,47167]) 
        self.assertEqual(macList["b4:ae:2b:67:62:ca","eth0.3"],[2945161,22555608])
     
    def test_parseDataMultiItemList_2(self):
        netMon = NetMonitor(self.logger) 
        value = ['b8:27:eb:f2:e0:33,28132,47167','94:de:80:ac:19:5e,50715,1028493','10:1c:0c:44:ca:9d,404,591','b4:ae:2b:67:62:ca,2945161,22555608,eth0.3','3c:a9:f4:38:76:0c,1654860,7959085','c0:1a:da:c0:36:c9,1324,521']
        netMon.parseData(value, False)
        self.assertEqual(len(macList),6)
        self.assertEqual(macList["b8:27:eb:f2:e0:33", "N/A"],[28132,47167]) 
        self.assertEqual(macList["b4:ae:2b:67:62:ca","eth0.3"],[2945161,22555608])


if __name__ == '__main__':
    unittest.main()
