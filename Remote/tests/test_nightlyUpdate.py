#!/usr/bin/python
from __future__ import print_function

import sqlite3
import unittest
import os
import sys
import time
import mock

sys.path.append('../')
from nmNightlyUpdate import dateFunctions
from nmNightlyUpdate import sqlClassExtend
from sqlClass import sqlAccess

TESTDB = "summarydb.sqlite"


# =====================================================================
class test_dateFunctions(unittest.TestCase):
    """Test our date functions class."""

    def setUp(self):
        pass
    
    def tearDown(self):
        pass

    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass


    # ======================================================
    # Test our getDateTomorrow method
    def test_01_basic_test(self):
        """Test a basic next day calculation."""
        q = dateFunctions()
        (year, month, day) = q.getDateTomorrow((2018, 2, 20))
        # Test written differently year to verify that the way items
        # are being captured in return variables is correct
        self.assertEqual(year, 2018)
        self.assertEqual(month, 2)
        self.assertEqual(day, 21)

    def test_02_leap_year(self):
        """Test a leap year next day calculation."""
        q = dateFunctions()
        res = q.getDateTomorrow((2016, 2, 28))
        self.assertEqual(res, (2016, 2, 29))

    def test_03_non_leap_year(self):
        """Test a non leap year next day calculation."""
        q = dateFunctions()
        res = q.getDateTomorrow((2018, 2, 28))
        self.assertEqual(res, (2018, 3, 1))

    def test_04_end_of_year(self):
        """Test an end of year loop around calculation."""
        q = dateFunctions()
        res = q.getDateTomorrow((2018, 12, 31))
        self.assertEqual(res, (2019, 1,1))        

    # ======================================================
    # Test our getDateYesterday method
    def test_10_basic_test(self):
        """Test a basic yesterday calculation."""
        q = dateFunctions()
        (year, month, day) = q.getDateYesterday((2018, 2, 20))
        # Test written differently year to verify that the way items
        # are being captured in return variables is correct
        self.assertEqual(year, 2018)
        self.assertEqual(month, 2)
        self.assertEqual(day, 19)

    def test_12_leap_year(self):
        """Test a leap year previous day calculation."""
        q = dateFunctions()
        res = q.getDateYesterday((2016, 3, 1))
        self.assertEqual(res, (2016, 2, 29))

    def test_13_non_leap_year(self):
        """Test a non leap year previous day calculation."""
        q = dateFunctions()
        res = q.getDateYesterday((2018, 3, 1))
        self.assertEqual(res, (2018, 2, 28))

    def test_14_beginning_of_year(self):
        """Test a beginning of year loop around calculation."""
        q = dateFunctions()
        res = q.getDateYesterday((2016,1,1))
        self.assertEqual(res, (2015, 12, 31)) 

    # ======================================================
    # Test our getDeltDays method
    def test_20_simpleDelta(self):
        """Calculcate a simple delta."""
        q = dateFunctions()
        res = q.getDeltaDays((2018, 3, 20), (2018, 3, 25))
        self.assertEqual(5, res)

    def test_21_simpleDelta(self):
        """Calculcate a leap year delta."""
        q = dateFunctions()
        res = q.getDeltaDays((2016, 2, 20), (2016, 3, 1))
        self.assertEqual(10, res)

    def test_22_simpleDelta(self):
        """Calculcate a non leap year delta."""
        q = dateFunctions()
        res = q.getDeltaDays((2018, 2, 20), (2018, 3, 1))
        self.assertEqual(9, res)

    def test_23_simpleDelta(self):
        """Calculcate an end of year delta."""
        q = dateFunctions()
        res = q.getDeltaDays((2018, 12, 20), (2019, 1, 5))
        self.assertEqual(16, res)




# =====================================================================
def buildDatabase():
    try:
        try:
            os.remove(TESTDB)
        except:
            pass
        db = sqlAccess(TESTDB)
        db.closeConnection()
    except:
        pass

     

# ---------------------------------------------------------------------
class extend_sqlClass(sqlClassExtend):
    """Class used for testing purposes"""


    def  indexValid(self):
        """Returns state (T/F) of index validity."""
        sql = "SELECT status FROM tblIndex WHERE parameter='Summary'"
        cur = self.conn.cursor()
        cur.execute(sql)
        row = cur.fetchall()
        if len(row) == 0 or row[0][0] != "valid":
            return False
        else:
            return True


    def get_indexInfo(self):
        """Returns a tuple with relevant index information."""
        sql = "SELECT lastIndex, status FROM tblIndex WHERE parameter='Summary'"
        cur = self.conn.cursor()
        try:
            cur.execute(sql)
            rows = cur.fetchall()
            if len(rows) == 1:
                index = rows[0][0]
                if rows[0][1] == "valid":
                    status = True
                else:
                    status = False
                return (index, status)

        except Exception, e:
            return (0, False)
               

    def __init__(self, dbName):
        super(extend_sqlClass, self).__init__(dbName)

class test_sqlClassExtend(unittest.TestCase):
    """Test our sqlClassExtend helper method calls."""

    def setUp(self):
        pass
    
    def tearDown(self):
        pass

    @classmethod
    def setUpClass(cls):
        buildDatabase()
        pass

    @classmethod
    def tearDownClass(cls):        
        pass

    # ======================================================
    # Test our getDateYesterday method
    def test_01_start(self):
        pass

    # ======================================================
    # Test our updateDailySummary method
    @mock.patch.object(sqlClassExtend,'_getDailyData')
    def test_10_updateDailySummary(self, myMock):
        """Write a new entry."""
        q = extend_sqlClass(TESTDB)
        myMock.return_value = (2000, 3000)
        # Confirm that lastSummaryIndex is pointing to 0
        ret = q._updateDailySummary(2018,6,1)
        self.assertEqual(ret, 1)
        myMock.assert_called_once_with(((2018, 6, 1)))

    @mock.patch.object(sqlClassExtend,'_getDailyData')
    def test_11_updateDailySummary(self, myMock):
        """Write a new entry."""
        q = extend_sqlClass(TESTDB)
        myMock.return_value = (3000, 4000)
        ret = q._updateDailySummary(2018,6,2)
        myMock.assert_called_once_with(((2018, 6, 2)))
        self.assertEqual(ret, 2)

    @mock.patch.object(sqlClassExtend, 'getDailySummaryId')
    @mock.patch.object(sqlClassExtend,'_getDailyData')
    def test_12_updateDailySummary(self, myMock, mock_getDSI):
        """Update an existing entry."""
        q = extend_sqlClass(TESTDB)
        myMock.return_value = (5000, 6000)
        mock_getDSI.return_value = 2
        ret = q._updateDailySummary(2018,6,2)
        myMock.assert_called_once_with(((2018, 6, 2)))
        self.assertEqual(2, ret)

    # ======================================================
    # Test our _invalidateIndex method
#    def test_20_invalidateIndex(self):
#        """Verify that index is invalidated."""
#        q = extend_sqlClass(TESTDB)
#        self.assertEqual(True, q.indexValid())
#        q._invalidateIndex()
#        self.assertEqual(False, q.indexValid())

    # ======================================================
    # Test our _saveIndex method
 #   def test_21_saveIndex(self):
 #       """Verify that we can save our index correctly."""
 #       q = extend_sqlClass(TESTDB)
 #       self.assertEqual(False, q.indexValid())
 #       q.set_lastSummaryIndex(2)
 #       q._saveIndex()
 #       val = q.get_indexInfo()
 #       self.assertEqual(2, val[0])
 #       self.assertEqual(True, val[1])

    # ======================================================
    # Test our _getDailyData method
    @mock.patch.object(sqlClassExtend, 'returnDayValues')
    def test_30_getDailySummary(self, myMock):
        """Verify that our function processes sums correctly."""
        retItems = ((1, 2000, 2000),(2, 3000, 2250), (3, 1298, 1200))
        q = extend_sqlClass(TESTDB)
        myMock.return_value = retItems
        ret = q._getDailyData((2018, 5, 3))
        myMock.assert_called_once_with(2018, 5, 3, True)
        self.assertEqual(ret, [6298, 5450])

    @mock.patch.object(sqlClassExtend, 'returnDayValues')
    def test_31_getDailySummary(self, myMock):
        """Verify that our function hadles None returns correctly."""
        q = extend_sqlClass(TESTDB)
        myMock.return_value = None
        ret = q._getDailyData((2017, 6, 4))
        myMock.assert_called_once_with(2017, 6, 4, True)
        self.assertEqual(ret, [0, 0])


# ---------------------------------------------------------------------
class extend_sqlClassIntegration(sqlClassExtend):
    """Class used for testing purposes""" 

    def set_indexStatusValid(self, val):
        self.indexStatusValid = val

    def set_lastSummaryIndex(self, val):
        self.lastSummaryIndex = val

    def addDateInfo(self, epochSecs):
        sql = "INSERT INTO tblDateInfo (EpochSecs) VALUES (%d) " % (epochSecs)
        self._writeRecord(sql)

    def __init__(self, dbName):
        super(extend_sqlClassIntegration, self).__init__(dbName)

# =====================================================================
def buildTestDatabase():
    try:
        try:
            os.remove(TESTDB)
        except:
            pass
        print("Building our test database")
        db = extend_sqlClassIntegration(TESTDB)
        db.addDateInfo(1532479525)  # 2018, 7, 24
        db.closeConnection()
        print("Test database build complete")

    except:
        pass   


# ---------------------------------------------------------------------
class test_sqlClassExtend_Main(unittest.TestCase):
    """Test our sqlClassExtend main method calls."""

    def setUp(self):
        pass
    
    def tearDown(self):
        pass

    @classmethod
    def setUpClass(cls):
        buildTestDatabase()
        pass

    @classmethod
    def tearDownClass(cls):        
        pass

    # ======================================================
    # Test our run (main) method

    def lastEntryDate_01(self):
        return ((2018, 3, 20))

    def dailySummary_01(self):
        pass

    # Our first test hit a blank database (no summary data).  The design calls for
    # the code to find the first data record in the date database and then work from
    # there to yesterday's date
    #
    # DO NOT MOCK _getFirstDateRecord() as we are testing that function to make
    # sure it correctly pulls data from the database
    @mock.patch.object(sqlClassExtend, '_getLastEntryDate')
    @mock.patch.object(sqlClassExtend, '_updateDailySummary')
    @mock.patch.object(dateFunctions,'getDateYesterday')
    def test_01_run_SingleEntryNew(self, mock_DY, mock_updateDS, mock_getLED):
        """No entries exist, this will be our first entry."""
        q = extend_sqlClassIntegration(TESTDB)
        mock_DY.return_value = (2018, 7, 25)
        mock_getLED.return_value = ((0, 0, 0))
        mock_updateDS.return_value = 1
        q.run()
        mock_DY.assert_called_once_with(((0, 0, 0)))
        mock_updateDS.assert_called_once_with(2018, 7, 24)


    @mock.patch.object(sqlClassExtend, '_getFirstDateRecord')
    @mock.patch.object(sqlClassExtend, '_getLastEntryDate')
    @mock.patch.object(sqlClassExtend, '_updateDailySummary')
    @mock.patch.object(dateFunctions,'getDateYesterday')
    def test_02_run_multipleEntry(self, mock_DY, mock_updateDS, mock_getLED, mock_getFDR):
        """Entries exist and we need to add more."""
        q = extend_sqlClassIntegration(TESTDB)
        mock_DY.return_value = (2018, 7, 26)
        mock_getLED.return_value = (2018, 7, 23)
        # Set up our return values for our 3 consecutive calls
        mock_updateDS.side_effect = [1, 2, 3]
        # Execute our tests
        q.run()
        # Verify our test executed correctly
        self.assertEqual(mock_getFDR.call_count, 0)
        mock_updateDS.assert_has_calls(
            [
                mock.call(2018,7,24),
                mock.call(2018,7,25),
                mock.call(2018,7,26)
            ],
        )
        self.assertEqual(mock_updateDS.call_count, 3)



if __name__ == '__main__':
    test_classes_to_run = [test_dateFunctions, test_sqlClassExtend, test_sqlClassExtend_Main]

    loader = unittest.TestLoader()
    suites_list = []
    for test_class in test_classes_to_run:
        suite = loader.loadTestsFromTestCase(test_class)
        suites_list.append(suite)

    full_suite = unittest.TestSuite(suites_list)
    runner = unittest.TextTestRunner()
    results = runner.run(full_suite)
    
    print(results)