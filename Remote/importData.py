#!/usr/bin/python
from __future__ import print_function

import socket
import time, sys
import select
import sqlClass
import logging
import logging.handlers
from mailer import Mailer
import smtplib
from netMonRcv import NetMonitor
from netMonRcv import macList

# TODO:  Put these items in a configuration file
dbName = "netMonitor.sqlite"


class errorLogger(object):
    """This class will print out errors to the screen."""
    
    def error(self, errorString):
        """Print out the errorString to the screen"""
        print(errorString)

    def debug(self, myString):
        """Print out the debug information."""
        print(myString)

    def __init__(self):
        pass


# -------------------------------------------------------------------
class trafficImporter(object):

    mailObj = None

    def dataStore(self, dictList, timestamp):
        # Add object for mailing 
        print("Storing items to database")
        print(dictList)
        q = sqlClass.sqlAccess(dbName, self.mailObj)
        q.storeData(dictList, timestamp)

    def setupLogging(self):
    ################### Set up our logging ########################
        netLogging = logging.getLogger('monitor.py')
        netLogging.setLevel(logging.DEBUG)

        handler = logging.handlers.SysLogHandler(address = '/dev/log')
        netLogging.addHandler(handler)
        return netLogging
    ###############################################################

    def _processTimeStamp(self, dateLine):
        """Process our timestamp here"""
        try:
            mins = int(dateLine.split(':')[1])
            if mins >= 0 and mins < 60:
                if mins % 15 == 0:
                    self.dataStore(macList, int(dateLine.split(';')[1]))
                    #self.netMon.printDictionary()
                    #print(int(dateLine.split(';')[1]))
                    self.netMon.clearDictionary()
        except Exception, e:
            print ("Exception processing " + dateLine + ":->" + str(e))

    # --------------------------------------------------------------------
    def _processLine(self, myLine):
        """Determine what needs to be done with this line of data."""
        if myLine.strip()[:9] == "TimeStamp":
            self._processTimeStamp(myLine.strip()[9:].strip())
        else:
            self.netMon.parseData(myLine)


    def run(self, fileName):

        with open(fileName) as fp:
            for cnt, line in enumerate(fp):
                self._processLine(line)



    def __init__(self):
        """Initialize our class global variables."""
        errorLogging = errorLogger()
        self.netMon = NetMonitor(errorLogging)


# -------------------------------------------------------------------
if __name__ == "__main__":
    importer = trafficImporter()
    importer.run("output.txt")



