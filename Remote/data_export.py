#!/usr/bin/env python
from __future__ import print_function

import sqlite3
import time
import json
import sys
import getopt
import support

class SqlAccess(object):
    """Class to access our sqlite database."""
    # --------------------------------------------------------------------
    def _create_database(self, connector):
        sql = "CREATE TABLE 'tblIndex' ('indexId' INTEGER PRIMARY KEY, \
        'lastIndex' INTEGER NOT NULL);"
        connector.execute(sql)
        sql = "INSERT INTO tblIndex (lastIndex) VALUES (0)"
        connector.execute(sql)
        connector.commit()

    # --------------------------------------------------------------------
    def _is_table_exists(self, connector):
        """Check if table for storing last index pulled, exists"""
        sql = "SELECT * FROM sqlite_master WHERE type='table' AND name='tblIndex'"
        if (not connector.execute(sql).fetchone()):
            self._create_database(connector)

    # --------------------------------------------------------------------
    def _store_last_index_read(self, connector):
        sql = "UPDATE tblIndex SET lastIndex = %s " % (self.lastIndex)
        sql += "WHERE indexId=1"
        connector.execute(sql)
        connector.commit()


    # --------------------------------------------------------------------
    def get_last_index(self, connector):
        sql = "SELECT lastIndex from tblIndex WHERE indexId=1"
        cur = connector.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        if len(rows) == 1:
            self.lastIndex = rows[0]

    # --------------------------------------------------------------------
    def get_epoch_secs(self, date_tuple):
        """Calculate the epoch secs for the last second of the date passed."""
        st = []
        st.extend(time.localtime())
        for i in range(0, 3):
            if i == 2:
                delta = st[i] - date_tuple[i] 
            st[i] = date_tuple[i]
        st[3] = 23
        st[4] = 59
        st[5] = 59
        st[7] -= delta
        last_item = int(time.mktime(st))

        st[3] = 0
        st[4] = 0
        st[5] = 0
        first_item = int(time.mktime(st))
        return first_item, last_item

    # -------------------------------------------------------------------------
    def _base_query(self):
        """Build the base query here."""
        sql = "SELECT tblData.dataId, tblData.dataIn, tblData.dataOut, tblLan.lan, tblLan.lanName, tblMac.MAC, tblMac.pcName, tblDateInfo.EpochSecs "
        sql += "FROM tblData "
        sql += "JOIN tblDateInfo, tblLan, tblMac "
        sql += "ON tblDateInfo.dateId = tblData.dateId "
        sql += "AND tblData.lanId = tblLan.lanId "
        sql += "AND tblData.macId = tblMac.macId "
        return sql
        

    # -------------------------------------------------------------------------
    def get_previous_data(self, date_tuple):
        """Returns all data up to and including date_tuple."""
        
        sql =  self._base_query()
        if date_tuple is not None:
            epoch_secs = self.get_epoch_secs(date_tuple)            
            sql += "WHERE "
            sql += "tblDateInfo.EpochSecs <=%s " % (epoch_secs[1])
        sql += "ORDER BY tblData.dataId"
        cur = self.conn.cursor()
        cur.execute(sql)
        myData = []
        rows = cur.fetchall()
        if rows:
            for i in rows:
                myData.append(i[1:])
            idx = len(rows) - 1
            self.lastIndex = rows[idx][0]
            self._store_last_index_read(self.conn)
            return myData
        return None

    # -------------------------------------------------------------------------
    def get_daily_data(self, date_tuple):
        """Returns data that exists on date_tuple date."""
        epoch_secs = self.get_epoch_secs(date_tuple)
        sql = self._base_query()
        sql += "WHERE tblDateInfo.EpochSecs >=%s AND tblDateInfo.EpochSecs <=%s " % (epoch_secs[0], epoch_secs[1])
        sql += "ORDER BY tblData.dataId"
        cur = self.conn.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        myData = []        
        if rows:
            for i in rows:
                myData.append(i[1:])
                self.lastIndex = i[0]
            self._store_last_index_read(self.conn)
            return myData
        return None

    # -------------------------------------------------------------------------
    def get_since_last_poll(self):
        """Returns data that exists on date_tuple date."""
        self.get_last_index(self.conn)
        sql = self._base_query()
        sql += "WHERE tblData.dataId > %s " % (self.lastIndex)
        sql += "ORDER BY tblData.dataId"
        print(sql)
        cur = self.conn.cursor()
        cur.execute(sql)
        rows = cur.fetchall()
        myData = []        
        if rows:
            for i in rows:
                myData.append(i[1:])
                self.lastIndex = i[0]
            self._store_last_index_read(self.conn)
            return myData
        return None


    # -------------------------------------------------------------------------
    def __init__(self, dbName=None):
        if dbName is not None:
            self.conn = sqlite3.connect(dbName)
            self.lastIndex = 0
            self._is_table_exists(self.conn)

def helpScreen():
    """Helpscreen to tell the user how to call this module."""
    print ('event_export.py -h [-a|-all] -l -f <filename>' )
    print ('-h : help')
    print ('-a : export the entire database (up to yesterday)')
    print ('-l: export new data (since last export')
    print ('-f <filename> : filename to use')

def main(argv):
    """Run main function of our script."""
    outfile = ""
    export_all = False
    export_since = False
    # yesterday is of format (YY, MM, DD)
    yesterday = support.getDateYesterday()

    try:
        opts, args = getopt.getopt(argv, "half:", ["all"])
    except getopt.GetoptError:
        helpScreen()
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            helpScreen()
            sys.exit()
        elif opt in ("-a", "-all"):
            export_all = True
        elif opt in ("-l"):
            export_since = True
        elif opt in ("-f"):
            print("loading file " + arg)
            outfile = arg

    print("using : " + outfile)
    sql = SqlAccess(outfile)

    if export_all is True:
        mylist = sql.get_previous_data(None)
    elif export_since is True:
        mylist = sql.get_since_last_poll()
    else:
        mylist = sql.get_daily_data(yesterday)

    if mylist is not None:
        try:
            with open('export.txt') as infile:
                json_data = json.load(infile)
                json_data.extend(mylist)
        except:
            json_data = []
            json_data.extend(mylist)
        with open('export.txt', 'w') as outfile:
            json.dump(json_data, outfile)        


# Main function call below this point
if __name__ == '__main__':
    main(sys.argv[1:])


