#!/usr/bin/env python
from __future__ import print_function

import sqlite3
import sqlClass
import sys
import datetime
import time
from configFile import DBASE
from ConfigParser import SafeConfigParser
from ConfigParser import NoOptionError


class sqlAccess_exend(sqlClass.sqlAccess):
    """Sql Access extionsion class."""

    def _getLastDateId(self):
        """Get the last date Id that will be deleted"""
        targetDate = datetime.datetime.now() - datetime.timedelta(days=self.history)
        targetDate = int(time.mktime(targetDate.timetuple()))
        sql = "SELECT dateId FROM tblDateInfo WHERE EpochSecs >= '%d' LIMIT 1" %(targetDate)
        rows = self._readDatabase(sql)
        if rows:
            return rows[0][0]

    # -------------------------------------------------------------------------
    def _deleteData(self, dataId):
        """Using the last index, remove all relevant data."""
        sql = "DELETE FROM tblDateInfo WHERE dateId <= '%d' " %(dataId)
        self._readDatabase(sql, True)
        sql = "DELETE FROM tblData WHERE dateId <= '%d' " %(dataId)
        self._readDatabase(sql, True)
        self.conn.execute("VACUUM")

    # -------------------------------------------------------------------------
    def loadValues(self):
        #Read our configuration file and set defaults if necessary
        parser = SafeConfigParser()
        parser.read('config.ini')

        try:
            self.history = (int)(parser.get('config', 'history'))
        except NoOptionError:
            pass

    # -------------------------------------------------------------------------
    def run(self):
        """Main class method."""
        dateId =  self._getLastDateId()
        self._deleteData(dateId)

    # -------------------------------------------------------------------------
    def __init__(self, dbName=None):
        if dbName is not None:
            self.conn = sqlite3.connect(dbName)
            self.history = 500
            self.loadValues()
            print(self.history)



def main(argv):
    """Run main function of our script."""
    print(DBASE)
    db = sqlAccess_exend(DBASE)
    db.run()



# Main function call below this point
if __name__ == '__main__':
    main(sys.argv[1:])            

